# Gate To Logic
Gates To Logic is a web application created as university team project. GTL is online logic gate simulator, where people can investigate the behaviour of logic gates and flip-flops.

## About

This is a project where a Spring REST API is secured using JSON Web Tokens and front is created with Angular framework.

## JSON Web Tokens
> JSON Web Tokens are an open, industry standard RFC 7519 method for representing claims securely between two parties.
for more info, check out https://jwt.io/

> Token authentication is a more modern approach and is designed solve problems session IDs stored server-side can’t. Using tokens in place of session IDs can lower your server load, streamline permission management, and provide better tools for supporting a distributed or cloud-based infrastructure.
>
> -- <cite>Stormpath</cite>


## Server side: Spring Boot

On the server side, the JWT signing is done in the /login REST call in UserController. The verification is done in a Filter (JwtRequestFilter). If a correct token isn't found an exception is thrown. If a correct token is found, the claims object is added to the Http Request object and can be used in any REST endpoint (as shown in ProjectController and PasswordController).

The heavy lifting for JWT signing is done by the more than excellent [Java JWT](https://github.com/jwtk/jjwt) library.

## Client Side: AngularJS

Our Angular app shows a login page with posibility 

## Running

It is a standard Maven project and can be imported into your favorite IDE. You run the example by starting the WebApplication class (it has a main) and navigating to http://localhost:4200/. If everything is correct you should see a "Gate To Logic!" message and a login form.

## Prerequisites

* Angular CLI 8.3.23
* Node v13.7.0
* Java 13.0.2
* Maven 3.6.3
* npm 6.13.6
* Docker

## Installation

``` 
npm install --save-dev @angular-devkit/build-angular
```
## Build Backend (SpringBoot Java)

```
# Maven Build : Navigate to the root folder where pom.xml is present 
mvn clean install
```

## Run application

### Start MongoDB

To run MongoDB on port 27017 server using Docker container use the following command:

```
docker run -d --rm --name mongo -v <localpath>:/data/db -p 27017:27017 mongo
```


### Start the API

```
java -jar spring-boot-backend-web/target/spring-boot-backend-web-0.0.1.jar

# the api will be running on port 8080
```

### Start the frontend app

```
ng serve

# the app will be running on port 4200
```


