import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl('',Validators.required),
    description: new FormControl()
  })

  constructor(public dialogRef: MatDialogRef<NewProjectComponent>) { }

  ngOnInit() {

  }

  createProject() {
    this.dialogRef.close(this.form.value)
  }

}
