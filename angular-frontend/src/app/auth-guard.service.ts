import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  canActivate() {
    console.log('auth',this.auth.isLoggedIn())
    if(this.auth.isLoggedIn()) return true
    this.router.navigate(['/login'])
  }

  constructor(private router: Router, private auth : AuthService) { }
}
