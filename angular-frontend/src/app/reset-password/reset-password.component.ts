import { ApiService } from './../api.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  send=false
  truereset=false
  code

  form = new FormGroup({
    email: new FormControl('',Validators.required)
  })
  trueform = new FormGroup({
    password: new FormControl('',Validators.required)
  })

  constructor(private route : ActivatedRoute, private api: ApiService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      if(params.has('code')){
        this.truereset=true
        this.code=params.get('code')
      }
    })
  }

  reset() {
    this.api.reset(this.form.get('email').value).subscribe(responce=>{
      if(responce['success']) {
        this.send = true
        console.log(this.send && !this.truereset)
      }
    })
  }
  trueresetf() {
    this.api.truereset({token:this.code,password: this.trueform.get('password').value}).subscribe(responce=>{
      console.log(responce)
    })
  }

}
