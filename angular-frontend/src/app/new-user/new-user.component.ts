import { ApiService } from './../api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  @ViewChild('repet',{static:false}) repet

  isError = false
  errortext=''
  send = false

  form = new FormGroup({
    login: new FormControl(),
    email: new FormControl(),
    password: new FormControl()
  })

  constructor(private api : ApiService) { }

  ngOnInit() {
  }

  register() {
    if(this.form.get('password').value==this.repet.nativeElement.value) {
      this.api.register(this.form.value).subscribe(responce=>{
        if(responce['success']) {
          this.send = true
        }
      },
      error=>{
        console.log(error)
        this.isError = true
        this.errortext = error.error.message
      })
    } else {
      this.isError = true
      this.errortext = 'hasła są różne'
    }
  }

}
