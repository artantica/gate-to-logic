import { AuthService } from './../auth.service';
import { ApiService } from './../api.service';
import { Project } from './../project';
import { NewProjectComponent } from './../new-project/new-project.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { stringify } from 'querystring';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  projects: MatTableDataSource<Project> = null

  constructor(private router: Router, private dialog: MatDialog, private api: ApiService, private auth: AuthService) { }

  ngOnInit() {
    this.api.getProjects().subscribe(responce => {
      console.log(responce)
      this.projects = new MatTableDataSource(responce)
      this.projects.paginator = this.paginator;
      this.projects.sort = this.sort;
    })
  }

  logout() {
    this.auth.logout()
    this.router.navigate(['login'])
  }

  newProject() {
    const dialogRef = this.dialog.open(NewProjectComponent, {
      width: '40%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return
      }
      result['representation'] = '{"elements":[], "wires":[]}'
      console.log(result)
      this.api.newProject(result).subscribe(r => {
        console.log(r)
        const data = this.projects.data;
        data.push(<Project>r);
        this.projects.data = data;
      })
    });
  }

  edit(id) {
    console.log(id)
    this.router.navigate(['project', id])
  }

}
