import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.css']
})
export class ActivateComponent implements OnInit {

  constructor(private route: ActivatedRoute,private router: Router,private api: ApiService) { }

  responcetxt = ''

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      this.api.activate(params.get('code'))
      .subscribe(responce=>{
        console.log(responce)
        this.responcetxt = responce['message'];
      },
      error=>{
        console.log(error,error['error'].message)
        this.responcetxt = error['error'].message;
      })
    })
  }

}
