import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InOutNameComponent } from './in-out-name.component';

describe('InOutNameComponent', () => {
  let component: InOutNameComponent;
  let fixture: ComponentFixture<InOutNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InOutNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InOutNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
