import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-in-out-name',
  templateUrl: './in-out-name.component.html',
  styleUrls: ['./in-out-name.component.css']
})
export class InOutNameComponent implements OnInit {

  text = ''

  form = new FormGroup({
    name: new FormControl('',Validators.required)
  })

  constructor(@Inject(MAT_DIALOG_DATA) public data: {in:boolean,name:string},public dialogRef: MatDialogRef<InOutNameComponent>) { }

  ngOnInit() {
    if(this.data.in) {
      this.text = 'inputu'
    } else {
      this.text = 'outputu'
    }
    this.form.get('name').setValue(this.data.name)
  }

  name() {
    this.dialogRef.close(this.form.value)
  }

}
