import { ExternPropertisComponent } from './../extern-propertis/extern-propertis.component';
import { InOutNameComponent } from './../in-out-name/in-out-name.component';
import { ApiService } from './../api.service';
import { ElementComponent } from './../elements/element/element.component';
import { browser } from 'protractor';
import { async } from '@angular/core/testing';
import { Validators } from '@angular/forms';
import { AndGateComponent } from './../elements/and-gate/and-gate.component';
import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList, Injectable, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSlider } from '@angular/material/slider';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit {

  @ViewChild('time', { static: true }) time: MatSlider
  viewx = 0
  viewy = 0
  showx = 0
  scale = 1
  width = window.innerWidth

  dragEE = new EventEmitter()

  system = { elements: [], wires: [] }
  // {
  //   elements: [
  //     { id: 0, type: 12, x: 10, y: 10, in: [], out: [0] },
  //     { id: 1, type: 12, x: 10, y: 25, in: [], out: [1] },
  //     { id: 2, type: 2, x: 25, y: 15, in: [0, 1], out: [2] },
  //     { id: 3, type: 3, x: 40, y: 15, in: [2], out: [3] },
  //     { id: 4, type: 12, x: 10, y: 40, in: [], out: [4] },
  //     { id: 5, type: 1, x: 55, y: 25, in: [3, 4], out: [5] },
  //     { id: 6, type: 3, x: 70, y: 25, in: [5], out: [undefined] },
  //   ],
  //   wires: [
  //     { id: 0, val: false, nextval: undefined, in: 0, out: 2 },
  //     { id: 1, val: false, nextval: undefined, in: 1, out: 2 },
  //     { id: 2, val: false, nextval: undefined, in: 2, out: 3 },
  //     { id: 3, val: false, nextval: undefined, in: 3, out: 5 },
  //     { id: 4, val: false, nextval: undefined, in: 4, out: 5 },
  //     { id: 5, val: false, nextval: undefined, in: 5, out: 6 },
  //   ]
  // }
  standardelements = [
    { id: 0, type: 1, x: 5, y: 5, in: [undefined, undefined], out: [undefined] },
    { id: 1, type: 2, x: 25, y: 5, in: [undefined, undefined], out: [undefined] },
    { id: 2, type: 3, x: 5, y: 25, in: [undefined], out: [undefined] },
    { id: 3, type: 4, x: 25, y: 25, in: [undefined, undefined], out: [undefined] },
    { id: 4, type: 5, x: 7, y: 45, in: [undefined, undefined], out: [undefined] },
    { id: 5, type: 6, x: 25, y: 45, in: [undefined, undefined], out: [undefined] },
    { id: 6, type: 7, x: 7, y: 65, in: [undefined, undefined], out: [undefined] },
    { id: 7, type: 8, x: 25, y: 65, in: [undefined, undefined, undefined], out: [undefined, undefined], val: false },
    { id: 8, type: 9, x: 5, y: 85, in: [undefined, undefined], out: [undefined, undefined], val: false },
    { id: 9, type: 10, x: 25, y: 85, in: [undefined, undefined], out: [undefined, undefined], val: false },
    { id: 10, type: 11, x: 5, y: 105, in: [undefined, undefined, undefined], out: [undefined, undefined], val: false },
    { id: 11, type: 12, x: 25, y: 105, in: [], out: [undefined], val: false },
    { id: 12, type: 13, x: 5, y: 125, in: [], out: [undefined], val: false },
    { id: 13, type: 14, x: 25, y: 125, in: [undefined], out: [], val: false },
    { id: 14, type: 15, x: 5, y: 145, in: [], out: [undefined] },
    { id: 15, type: 16, x: 25, y: 145, in: [undefined], out: [] },
    { id: 16, type: 17, x: 5, y: 165, in: [], out: [] },
    { id: 17, type: 18, x: 25, y: 165, in: [undefined], out: [undefined, undefined] }
  ]

  constructor(private route: ActivatedRoute, private api: ApiService, private dialog: MatDialog) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params => {
        if (params.has('id')) {
          this.api.getProject(params.get('id')).subscribe(responce => {
            console.log(responce)
            this.system = JSON.parse(responce['representation'])
          })
        }
      }
    )
    setTimeout(this.timedstep.bind(this), 500)
    // setInterval(this.step.bind(this),500)
  }

  timedstep() {
    if (this.time.value != 0) {
      this.step(this.system)
      setTimeout(this.timedstep.bind(this), 1000 / this.time.value)
    } else {
      setTimeout(this.timedstep.bind(this), 100)
    }

  }
  savesucces = false;
  save() {
    this.route.paramMap.subscribe(
      params => {
        this.api.saveProject(params.get('id'), JSON.stringify(this.system)).subscribe(r => {
          console.log(r)
          this.savesucces = true
          setTimeout(()=>this.savesucces=false,1000)
        })
      })
  }

  wheel($event) {
    // console.log($event.deltaY)
    this.showx = this.showx + $event.deltaY
    if (this.showx < 0) {
      this.showx = 0
    } else if (this.showx > 90) {
      this.showx = 90
    }
  }

  arguments(system, _input, _input_number) {
    let _in = [];
    for (let i = 0; i < _input_number; i++) {
      let a = _input[i] != null ? system.wires[_input[i]] : null;
      let val = a != null ? a.val : false;
      _in.push(val)
    }
    return _in
  }
  result(system, _vals, _output, _output_number) {
    for (let i = 0; i < _output_number; i++) {
      if (_output[i] != null && system.wires[_output[i]] != null) {
        system.wires[_output[i]].nextval = _vals[i]
      }
    }
  }

  get_result(_out) {
    return this.system.wires[_out].val;
  }

  step(system) {
    system.elements.forEach(e => {
      if (e != undefined) {
        let _in
        switch (e.type) {
          // AND
          case 1:
            _in = this.arguments(system, e.in, 2);
            this.result(system, [_in[0] && _in[1]], e.out, 1)
            break;;
          // OR
          case 2:
            _in = this.arguments(system, e.in, 2);
            this.result(system, [_in[0] || _in[1]], e.out, 1);
            break;
          // NOT
          case 3:
            _in = this.arguments(system, e.in, 1);
            this.result(system, [!_in[0]], e.out, 1);
            break;
          // XOR
          case 4:
            _in = this.arguments(system, e.in, 2);
            this.result(system, [xor(_in[0], _in[1])], e.out, 1);
            break;
          // NAND
          case 5:
            _in = this.arguments(system, e.in, 2);
            this.result(system, [nand(_in[0], _in[1])], e.out, 1);
            break;
          // NOR
          case 6:
            _in = this.arguments(system, e.in, 2);
            this.result(system, [nor(_in[0], _in[1])], e.out, 1);
            break;
          // XNOR
          case 7:
            _in = this.arguments(system, e.in, 2);
            this.result(system, [xnor(_in[0], _in[1])], e.out, 1);
            break;
          // RS
          case 8:
            _in = this.arguments(system, e.in, 3);
            e.val = rs(_in[0], _in[1], _in[2], e.val);
            this.result(system, [e.val, !e.val], e.out, 2);
            break;
          // D
          case 9:
            _in = this.arguments(system, e.in, 2);
            this.result(system, [e.val, !e.val], e.out, 2);
            e.val = d(_in[0], _in[1], e.val);
            break;
          // T
          case 10:
            _in = this.arguments(system, e.in, 2);
            e.val = t(_in[0], _in[1], e.val);
            this.result(system, [e.val, !e.val], e.out, 2);
            break;
          // JK
          case 11:
            _in = this.arguments(system, e.in, 3);
            e.val = jk(_in[0], _in[1], _in[2], e.val);
            this.result(system, [e.val, !e.val], e.out, 2);
            break;
          // TOGGLE INPUT
          case 12:
            this.result(system, [e.val], e.out, 1)
            break;
          // CLOCK INPUT
          case 13:
            this.result(system, [e.val], e.out, 1)
            e.val = !e.val
            break;
          // LIGHT OUTPUT
          case 14:
            _in = this.arguments(system, e.in, 1);
            e.val = _in[0]
            break;
          case 15:
            this.result(system, [e.val], e.out, 1)
            break;
          case 16:
            _in = this.arguments(system, e.in, 1);
            e.val = _in[0]
            break;
          case 17:
            _in = this.arguments(system, e.in, e.in.length);
            // console.log(_in, e.connect.system)
            for (let i = 0; i < e.connect.inputs.length; i++) {
              for (let j = 0; j < e.connect.system.elements.length; j++) {
                if (e.connect.system.elements[j] && e.connect.inputs[i] == e.connect.system.elements[j].id) {
                  e.connect.system.elements[j].val = _in[i]
                }
              }
            }
            this.step(e.connect.system)
            let res = []
            for (let i = 0; i < e.connect.outputs.length; i++) {
              for (let j = 0; j < e.connect.system.elements.length; j++) {
                if (e.connect.system.elements[j] && e.connect.outputs[i] == e.connect.system.elements[j].id) {
                  res.push(e.connect.system.elements[j].val)
                }
              }
            }
            // console.log(res)
            this.result(system, res, e.out, e.out.length)
            break;
          case 18:
            _in = this.arguments(system, e.in, 1);
            this.result(system, [_in[0],_in[0]], e.out, 2)
            break;
        }
      }
    })
    system.wires.forEach(w => {
      if (w != undefined) {
        w.val = w.nextval
      }
    })
  }

  deleteElement(id) {
    this.system.elements[id].in.forEach(wid => {
      if (wid != null) {
        let w = this.system.wires[wid]
        this.system.elements[w.in].out = this.system.elements[w.in].out.map(e => {
          if (e == wid) return undefined;
          return e
        })
        this.system.wires[wid] = undefined
      }
    })
    this.system.elements[id].out.forEach(wid => {
      if (wid != null) {
        let w = this.system.wires[wid]
        this.system.elements[w.out].in = this.system.elements[w.out].in.map(e => {
          if (e == wid) return undefined;
          return e
        })
        this.system.wires[wid] = undefined
      }
    })
    this.system.elements[id] = undefined
  }

  startx
  starty
  mousex
  mousey
  dx = 0
  dy = 0
  object = undefined
  tryb = 1
  cable
  move = false
  startDrag($event) {
    // console.log(document.elementFromPoint($event.x, $event.y))
    this.mousex = $event.x
    this.mousey = $event.y
    this.dx = 0
    this.dy = 0
    if ($event.x > 0.8 * 0.2 * this.width) {
      this.move = true
    }
  }
  endDrag($event) {
    this.mousex = undefined
    this.mousey = undefined
    this.dragEE.emit({ 'dx': undefined, 'dy': undefined })
    if (this.tryb == 4 && this.move) {
      this.viewx = this.viewx + this.dx
      this.viewy = this.viewy + this.dy
      this.dx = 0
      this.dy = 0
    }
    this.move = false
  }
  drag($event) {
    if (this.mousex == undefined || this.mousey == undefined) {
      this.dx = 0
      this.dy = 0
    } else {
      this.dx = 160 * (($event.x - this.mousex) / (window.innerWidth * 0.64))
      this.dy = 90 * (($event.y - this.mousey) / (window.innerWidth * 0.36))
    }
    this.dragEE.emit({ 'dx': this.dx, 'dy': this.dy })
    if (this.object) {
      if (this.object.startx) {
        this.object.x = this.object.startx + this.dx
        this.object.y = this.object.starty + this.dy
      }
    }
  }
  cableChange(c) {
    this.cable = c
    if (c && c.in != undefined && c.out != undefined) {
      let wid = this.system.elements[this.cable.out].in[this.cable.outcable]
      if (wid != null) {
        let w = this.system.wires[wid]
        this.system.elements[w.in].out = this.system.elements[w.in].out.map(e => {
          if (e == wid) return undefined;
          return e
        })
        this.system.wires[wid] = undefined
      }
      wid = this.system.elements[this.cable.in].out[this.cable.incable]
      if (wid != null) {
        let w = this.system.wires[wid]
        this.system.elements[w.out].in = this.system.elements[w.out].in.map(e => {
          if (e == wid) return undefined;
          return e
        })
        this.system.wires[wid] = undefined
      }
      if (this.system.wires.indexOf(undefined) == -1) {
        this.system.wires.push(undefined)
      }
      let nid = this.system.wires.indexOf(undefined)
      this.system.wires[nid] = { id: nid, val: false, nextval: undefined, in: this.cable.in, out: this.cable.out }
      this.system.elements[this.cable.out].in[this.cable.outcable] = nid
      this.system.elements[this.cable.in].out[this.cable.incable] = nid
      this.cable = undefined
    }
  }
  startDragS($event, id, x, y) {
    this.mousex = $event.x
    this.mousey = $event.y
    this.object = JSON.parse(JSON.stringify(this.standardelements[id]))
    this.object.startx = this.standardelements[id].x
    this.object.starty = this.standardelements[id].y - this.showx
    this.object.x = this.standardelements[id].x
    this.object.y = this.standardelements[id].y - this.showx
  }
  endDragS($event) {
    if (this.object.x > 40) {
      delete (this.object.startx)
      delete (this.object.starty)
      let realx = (this.object.x - 40 - this.viewx) / this.scale
      let realy = (this.object.y - this.viewy) / this.scale
      // console.log(JSON.stringify(this.object), realx, realy)
      if (this.object.type == 15 || this.object.type == 16) {
        const dialogRef = this.dialog.open(InOutNameComponent, {
          data: { in: this.object.type == 15, name: '' },
          width: '40%',
        });

        dialogRef.afterClosed().subscribe(result => {
          if (!result) {
            this.object = undefined
            return
          }
          let name = result.name
          // console.log(name, this.innames)
          if (this.object.type == 15 && this.innames.indexOf(name) >= 0) {
            this.object = undefined
            return
          } else if (this.object.type == 16 && this.outnames.indexOf(name) >= 0) {
            this.object = undefined
            return
          }
          // console.log(this.innames)
          this.object.name = name
          if (this.system.elements.indexOf(undefined) == -1) {
            this.system.elements.push(undefined)
          }
          let nid = this.system.elements.indexOf(undefined)
          this.object.id = nid
          this.object.x = realx
          this.object.y = realy
          this.system.elements[nid] = this.object
          this.object = undefined
        });
      } else if (this.object.type == 17) {
        const dialogRef = this.dialog.open(ExternPropertisComponent, {
          data: {},
          width: '40%',
        });

        dialogRef.afterClosed().subscribe(result => {
          if (!result) {
            this.object = undefined
            return
          }
          this.object.connect = result
          for (let e in result.inputs) {
            this.object.in.push(undefined)
          }
          for (let e in result.outputs) {
            this.object.out.push(undefined)
          }
          if (this.system.elements.indexOf(undefined) == -1) {
            this.system.elements.push(undefined)
          }
          let nid = this.system.elements.indexOf(undefined)
          this.object.id = nid
          this.object.x = realx
          this.object.y = realy
          this.system.elements[nid] = this.object
          this.object = undefined
        })
      } else {
        this.object.x = realx
        this.object.y = realy
        if (this.system.elements.indexOf(undefined) == -1) {
          this.system.elements.push(undefined)
        }
        let nid = this.system.elements.indexOf(undefined)
        this.object.id = nid
        this.system.elements[nid] = this.object
      }
    }
    if (this.object.type != 15 && this.object.type != 16 && this.object.type != 17) {
      this.object = undefined
    }
    // console.log(this.system)
  }
  get wirestab() {
    return this.system.wires.filter(e => { return e != undefined })
  }
  get elementstab() {
    return this.system.elements.filter(e => { return e != undefined })
  }
  get standardtab() {
    return this.standardelements.filter(e => { return e != undefined })
  }
  get innames(): string[] {
    return this.system.elements.filter(e => {
      return e && e.type == 15
    }).map(e => {
      return e.name
    })
  }
  get outnames(): string[] {
    return this.system.elements.filter(e => {
      return e && e.type == 16
    }).map(e => {
      return e.name
    })
  }
}

function xor(a, b) {
  return a !== b;
}

function nand(a, b) {
  return !(a && b);
}

function nor(a, b) {
  return !(a || b);
}

function xnor(a, b) {
  return !xor(a, b);
}

function rs(R, clock, S, Q) {
  if (clock) {
    if (S && R) {
      Q = undefined; //Unknown
    } else if (!S && R) {
      Q = false;
    } else if (S && !R) {
      Q = true;
    }
  }
  // console.log(R, clock, S, Q)
  return Q;
}

function d(D, clock, Q) {
  if (clock) {
    Q = D;
  }
  return Q;
}

function t(T, clock, Q) {
  if (clock && T) {
    Q = !Q;
  }
  return Q;
}

function jk(J, clock, K, Q) {
  if (clock) {
    if (J && K) {
      Q = !Q;
    } else if (!J && K) {
      Q = 0;
    } else if (J && !K) {
      Q = 1;
    }
  }
  return Q;
}
