import { Directive, Input, HostListener, EventEmitter, Output } from '@angular/core';
import { OnInit } from '@angular/core';

@Directive({
  selector: '[appDragDrop]'
})
export class DragDropDirective implements OnInit {

  @Input() element
  @Input() tryb
  @Input() scale
  @Input() drag: EventEmitter<unknown>
  @Input() cable
  @Input() viewx
  @Input() viewy
  @Output() cableChange = new EventEmitter()
  @Output() delete = new EventEmitter()

  constructor() { }

  ngOnInit() {
    this.drag.subscribe(e => {
      if (this.start) {
        if(e.dx == undefined && e.dy == undefined) {
          this.start = false;
        } else {
          this.element.x = this.startx + e.dx / this.scale
          this.element.y = this.starty + e.dy / this.scale
        }
      }
    })
  }

  startx
  starty
  start = false
  cablein
  cableid

  @HostListener('mousedown', ['$event'])
  startDrag($event) {
    // console.log($event)
    this.startx = this.element.x
    this.starty = this.element.y
    if (this.tryb == 1) {
      this.start = true
    } else if (this.tryb == 3) {
      this.delete.emit(this.element.id)
    }
    else if (this.tryb == 2) {
      let c = { x: undefined, y: undefined, in: undefined, incable: undefined, out: undefined, outcable: undefined }
      let px = (160 * ($event.x / (window.innerWidth * 0.64)) - 40 - this.viewx)/this.scale - this.element.x
      let py = (90 * ($event.y / (window.innerWidth * 0.36)) - this.viewy)/this.scale - this.element.y
      if (px < 5) {
        c.x = this.element.x
        let p = 0
        let tmp = 15 / (this.element.in.length + 1)
        while (py > tmp) {
          p++
          tmp += 10 / (this.element.in.length + 1)
        }
        if (p == this.element.in.length) p--;
        c.y = this.element.y + (p + 1) * (10 / (this.element.in.length + 1))
        c.out = this.element.id
        c.outcable = p
      } else {
        c.x = this.element.x + 10
        let p = 0
        let tmp = 15 / (this.element.out.length + 1)
        while (py > tmp) {
          p++
          tmp += 10 / (this.element.in.length + 1)
        }
        if (p == this.element.out.length) p--;
        c.y = this.element.y + (p + 1) * (10 / (this.element.out.length + 1))
        c.in = this.element.id
        c.incable = p
      }
      this.cableChange.emit(c)
    }
  }
  @HostListener('mouseup', ['$event'])
  endDrag($event) {
    this.startx = undefined
    this.starty = undefined
    if (this.tryb == 1) {
      this.start = false
    } else if (this.tryb == 2 && this.cable) {
      let px = (160 * ($event.x / (window.innerWidth * 0.64)) - 40 - this.viewx)/this.scale - this.element.x
      let py = (90 * ($event.y / (window.innerWidth * 0.36)) - this.viewy)/this.scale - this.element.y
      let p = 0
      let secin
      if (px < 5) {
        secin = true
        let tmp = 15 / (this.element.in.length + 1)
        while (py > tmp) {
          p++
          tmp += 10 / (this.element.in.length + 1)
        }
        if (p == this.element.in.length) p--;
      } else {
        secin = false
        let tmp = 15 / (this.element.out.length + 1)
        while (py > tmp) {
          p++
          tmp += 10 / (this.element.in.length + 1)
        }
        if (p == this.element.out.length) p--;
      }
      if (this.cable.in != undefined && secin) {
        this.cable.out = this.element.id
        this.cable.outcable = p
        this.cableChange.emit(this.cable)
      } else if (this.cable.out != undefined && !secin) {
        this.cable.in = this.element.id
        this.cable.incable = p
        this.cableChange.emit(this.cable)
      } else {
        this.cableChange.emit(undefined)
      }
    }
  }
}
