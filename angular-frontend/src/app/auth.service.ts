import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public jwtHelper: JwtHelperService = new JwtHelperService()

  constructor(private http : HttpClient) { }

  login(credentials) {
    return this.http.post("http://127.0.0.1:8080/login",credentials)
  }
  logout() {
    localStorage.removeItem("token")
  }
  isLoggedIn() {
    return localStorage.getItem('token')&&this.jwtHelper.isTokenExpired()
  }
  get username() {
    let token = localStorage.getItem("token")
    if(!token) return null
    return this.jwtHelper.decodeToken(token).sub
  }
}
