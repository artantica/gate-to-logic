import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  state=1
  buttontext="Nowe konto"

  constructor(private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    if(localStorage.getItem('token'))this.router.navigate(['/user'])
    this.route.paramMap.subscribe(
      params=>{
        if(!params.has('state')) {
          this.buttontext = "Nowe konto"
          this.state=1
        } else if(params.get('state')=='login') {
          this.buttontext = "Nowe konto"
          this.state=1
        } else if(params.get('state')=='register') {
          this.buttontext = "Logowanie"
          this.state=2
        }
        else if(params.get('state')=='reset') {
          this.buttontext = "Logowanie"
          this.state=3
        }
      }
    )
  }

}
