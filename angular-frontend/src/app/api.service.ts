import { Project } from './project';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  getProjects() {
    return this.httpClient.get<Project[]>("http://127.0.0.1:8080/project/all/")
  }
  getProject(id : string) {
    return this.httpClient.get("http://127.0.0.1:8080/project/get/"+id)
  }
  saveProject(id, representation) {
    return this.httpClient.patch("http://127.0.0.1:8080/project/save/"+id,representation)
  }
  newProject(project) {
    return this.httpClient.post("http://127.0.0.1:8080/project/new",project)
  }
  register(data) {
    return this.httpClient.post("http://127.0.0.1:8080/register",data)
  }
  reset(email) {
    return this.httpClient.post('http://127.0.0.1:8080/forgot-password',email)
  }
  truereset(password) {
    return this.httpClient.post('http://127.0.0.1:8080/reset',password)
  }
  activate(code) {
    return this.httpClient.post('http://127.0.0.1:8080/verify-mail',code)
  }
}
