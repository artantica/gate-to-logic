import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    login : new FormControl('',Validators.required),
    password: new FormControl('',Validators.required)
  });
  isError = false
  errortext = ''

  hover=false

  constructor(private router: Router, private auth : AuthService) { }

  ngOnInit() {
  }

  login() {
    this.auth.login(this.form.value).subscribe(responce=>{
      console.log(responce)
      if(responce && responce['jwtAccessToken']) {
        localStorage.setItem("token",responce['jwtAccessToken'])
        this.router.navigate(['user'])
      }
    },
    error=>{
      this.isError = true
      this.errortext = error['error'].message;

      console.log(error)
    })
  }

}
