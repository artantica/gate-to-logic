import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-fork]',
  templateUrl: './fork.component.html',
  styleUrls: ['./fork.component.css']
})
export class ForkComponent implements OnInit {

  @Input() element

  constructor() { }

  ngOnInit() {
  }

}
