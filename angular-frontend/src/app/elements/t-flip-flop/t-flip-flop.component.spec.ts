import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TFlipFlopComponent } from './t-flip-flop.component';

describe('TFlipFlopComponent', () => {
  let component: TFlipFlopComponent;
  let fixture: ComponentFixture<TFlipFlopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TFlipFlopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TFlipFlopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
