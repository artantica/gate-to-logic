import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-t-flip-flop]',
  templateUrl: './t-flip-flop.component.html',
  styleUrls: ['./t-flip-flop.component.css']
})
export class TFlipFlopComponent implements OnInit {

  @Input() element

  constructor() { }

  ngOnInit() {
  }

}
