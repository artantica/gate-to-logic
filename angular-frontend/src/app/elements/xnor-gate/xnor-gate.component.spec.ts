import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XnorGateComponent } from './xnor-gate.component';

describe('XnorGateComponent', () => {
  let component: XnorGateComponent;
  let fixture: ComponentFixture<XnorGateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XnorGateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XnorGateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
