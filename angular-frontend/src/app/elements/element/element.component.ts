import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-element]',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.css']
})
export class ElementComponent implements OnInit {

  @Input() element
  @Input() system
  @Output() change  = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  _change(e) {
    this.change.emit(e)
  }

}
