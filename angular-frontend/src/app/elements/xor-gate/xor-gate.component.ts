import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-xor-gate]',
  templateUrl: './xor-gate.component.html',
  styleUrls: ['./xor-gate.component.css']
})
export class XorGateComponent implements OnInit {

  @Input() element

  constructor() { }

  ngOnInit() {
  }

}
