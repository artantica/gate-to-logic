import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NandGateComponent } from './nand-gate.component';

describe('NandGateComponent', () => {
  let component: NandGateComponent;
  let fixture: ComponentFixture<NandGateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NandGateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NandGateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
