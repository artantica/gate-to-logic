import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-nand-gate]',
  templateUrl: './nand-gate.component.html',
  styleUrls: ['./nand-gate.component.css']
})
export class NandGateComponent implements OnInit {

  @Input() element

  constructor() { }

  ngOnInit() {
  }

}
