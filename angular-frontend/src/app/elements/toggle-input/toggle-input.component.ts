import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-toggle-input]',
  templateUrl: './toggle-input.component.html',
  styleUrls: ['./toggle-input.component.css']
})
export class ToggleInputComponent implements OnInit {

  @Input() element
  @Output() change  = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  _change() {
    let e = this.element
    e.val = !e.val
    this.change.emit(e)
  }

}
