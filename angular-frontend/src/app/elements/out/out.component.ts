import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { InOutNameComponent } from 'src/app/in-out-name/in-out-name.component';

@Component({
  selector: '[app-out]',
  templateUrl: './out.component.html',
  styleUrls: ['./out.component.css']
})
export class OutComponent implements OnInit {

  @Input() element
  @Input() system

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  changeName() {
    const dialogRef = this.dialog.open(InOutNameComponent, {
      data: {in: true,name:this.element.name},
      width: '40%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return
      }
      let name = result.name
      let innames = this.system.elements.filter(e=>{
        return e&&e.type==15
      }).map(e=>{
        return e.name
      })
      if(innames.indexOf(name)>=0) {
        return
      }
      this.element.name = result.name
    })
  }

}
