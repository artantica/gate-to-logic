import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsFlipFlopComponent } from './rs-flip-flop.component';

describe('RsFlipFlopComponent', () => {
  let component: RsFlipFlopComponent;
  let fixture: ComponentFixture<RsFlipFlopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsFlipFlopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsFlipFlopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
