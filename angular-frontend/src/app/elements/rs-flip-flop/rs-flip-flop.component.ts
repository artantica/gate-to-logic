import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-rs-flip-flop]',
  templateUrl: './rs-flip-flop.component.html',
  styleUrls: ['./rs-flip-flop.component.css']
})
export class RsFlipFlopComponent implements OnInit {

  @Input() element

  constructor() { }

  ngOnInit() {
  }

}
