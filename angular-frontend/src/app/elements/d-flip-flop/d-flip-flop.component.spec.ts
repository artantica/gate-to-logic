import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DFlipFlopComponent } from './d-flip-flop.component';

describe('DFlipFlopComponent', () => {
  let component: DFlipFlopComponent;
  let fixture: ComponentFixture<DFlipFlopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DFlipFlopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DFlipFlopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
