import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-d-flip-flop]',
  templateUrl: './d-flip-flop.component.html',
  styleUrls: ['./d-flip-flop.component.css']
})
export class DFlipFlopComponent implements OnInit {

  @Input() element

  constructor() { }

  ngOnInit() {
  }

}
