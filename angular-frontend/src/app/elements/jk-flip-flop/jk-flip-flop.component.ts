import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-jk-flip-flop]',
  templateUrl: './jk-flip-flop.component.html',
  styleUrls: ['./jk-flip-flop.component.css']
})
export class JkFlipFlopComponent implements OnInit {

  @Input() element

  constructor() { }

  ngOnInit() {
  }

}
