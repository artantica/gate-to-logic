import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JkFlipFlopComponent } from './jk-flip-flop.component';

describe('JkFlipFlopComponent', () => {
  let component: JkFlipFlopComponent;
  let fixture: ComponentFixture<JkFlipFlopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JkFlipFlopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JkFlipFlopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
