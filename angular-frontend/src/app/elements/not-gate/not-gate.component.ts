import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-not-gate]',
  templateUrl: './not-gate.component.html',
  styleUrls: ['./not-gate.component.css']
})
export class NotGateComponent implements OnInit {

  @Input() element
  
  constructor() { }

  ngOnInit() {
  }

}
