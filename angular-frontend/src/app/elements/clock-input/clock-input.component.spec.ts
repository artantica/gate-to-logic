import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClockInputComponent } from './clock-input.component';

describe('ClockInputComponent', () => {
  let component: ClockInputComponent;
  let fixture: ComponentFixture<ClockInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClockInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClockInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
