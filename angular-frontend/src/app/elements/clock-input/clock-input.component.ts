import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-clock-input]',
  templateUrl: './clock-input.component.html',
  styleUrls: ['./clock-input.component.css']
})
export class ClockInputComponent implements OnInit {

  @Input() element

  constructor() { }

  ngOnInit() {
  }

}
