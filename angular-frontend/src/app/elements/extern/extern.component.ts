import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-extern]',
  templateUrl: './extern.component.html',
  styleUrls: ['./extern.component.css']
})
export class ExternComponent implements OnInit {

  @Input() element

  constructor() { }

  ngOnInit() {
  }

}
