import { Component, OnInit, Input } from '@angular/core';
import { InOutNameComponent } from 'src/app/in-out-name/in-out-name.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: '[app-in]',
  templateUrl: './in.component.html',
  styleUrls: ['./in.component.css']
})
export class InComponent implements OnInit {

  @Input() element
  @Input() system

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  changeName() {
    const dialogRef = this.dialog.open(InOutNameComponent, {
      data: {in: true,name:this.element.name},
      width: '40%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return
      }
      let name = result.name
      let innames = this.system.elements.filter(e=>{
        return e&&e.type==15
      }).map(e=>{
        return e.name
      })
      if(innames.indexOf(name)>=0) {
        return
      }
      this.element.name = result.name
    })
  }

}
