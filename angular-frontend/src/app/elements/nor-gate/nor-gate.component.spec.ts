import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NorGateComponent } from './nor-gate.component';

describe('NorGateComponent', () => {
  let component: NorGateComponent;
  let fixture: ComponentFixture<NorGateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NorGateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NorGateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
