import { AuthGuardService } from './auth-guard.service';
import { WelcomeComponent } from './welcome/welcome.component';
import {RouterModule} from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { LoginComponent } from './login/login.component';
import { NewUserComponent } from './new-user/new-user.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UserComponent } from './user/user.component';
import { NewProjectComponent } from './new-project/new-project.component';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { AndGateComponent } from './elements/and-gate/and-gate.component';
import { ElementComponent } from './elements/element/element.component';
import { OrGateComponent } from './elements/or-gate/or-gate.component';
import { NotGateComponent } from './elements/not-gate/not-gate.component';
import { ToggleInputComponent } from './elements/toggle-input/toggle-input.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DragDropDirective } from './directives/drag-drop.directive';
import { XorGateComponent } from './elements/xor-gate/xor-gate.component';
import { NandGateComponent } from './elements/nand-gate/nand-gate.component';
import { NorGateComponent } from './elements/nor-gate/nor-gate.component';
import { XnorGateComponent } from './elements/xnor-gate/xnor-gate.component';
import { RsFlipFlopComponent } from './elements/rs-flip-flop/rs-flip-flop.component';
import { DFlipFlopComponent } from './elements/d-flip-flop/d-flip-flop.component';
import { TFlipFlopComponent } from './elements/t-flip-flop/t-flip-flop.component';
import { JkFlipFlopComponent } from './elements/jk-flip-flop/jk-flip-flop.component';
import { ClockInputComponent } from './elements/clock-input/clock-input.component';
import { JwtModule } from '@auth0/angular-jwt';
import { ActivateComponent } from './activate/activate.component';
import {MatSliderModule} from '@angular/material/slider';
import { OutputComponent } from './elements/output/output.component';
import { InComponent } from './elements/in/in.component';
import { OutComponent } from './elements/out/out.component';
import { InOutNameComponent } from './in-out-name/in-out-name.component';
import { ExternComponent } from './elements/extern/extern.component';
import { ExternPropertisComponent } from './extern-propertis/extern-propertis.component';
import { MatSelectModule } from '@angular/material';
import { ForkComponent } from './elements/fork/fork.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginComponent,
    NewUserComponent,
    ResetPasswordComponent,
    UserComponent,
    NewProjectComponent,
    ProjectEditComponent,
    AndGateComponent,
    ElementComponent,
    OrGateComponent,
    NotGateComponent,
    ToggleInputComponent,
    DragDropDirective,
    XorGateComponent,
    NandGateComponent,
    NorGateComponent,
    XnorGateComponent,
    RsFlipFlopComponent,
    DFlipFlopComponent,
    TFlipFlopComponent,
    JkFlipFlopComponent,
    ClockInputComponent,
    ActivateComponent,
    OutputComponent,
    InComponent,
    OutComponent,
    InOutNameComponent,
    ExternComponent,
    ExternPropertisComponent,
    ForkComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: '', component: WelcomeComponent},
      {path: 'user', component: UserComponent, canActivate: [AuthGuardService]},
      {path: 'project/:id', component: ProjectEditComponent, canActivate: [AuthGuardService]},
      {path: 'activate/:code', component: ActivateComponent},
      {path: 'reset/:code', component: ResetPasswordComponent},
      {path: ':state', component: WelcomeComponent}
    ]),
    BrowserAnimationsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatIconModule,
    MatTooltipModule,
    DragDropModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: ()=> {
          let token = localStorage.getItem('token')
          if(!token) {
            return ''
          }
          return token
        },
        whitelistedDomains: ["127.0.0.1:8080"],
        blacklistedRoutes: []
      }
    }),
    MatSliderModule,
    MatSelectModule,
    DragDropModule
  ],
  providers: [
    AuthGuardService
  ],
  entryComponents: [
    NewProjectComponent,
    InOutNameComponent,
    ExternPropertisComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
