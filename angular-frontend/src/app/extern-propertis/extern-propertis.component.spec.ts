import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternPropertisComponent } from './extern-propertis.component';

describe('ExternPropertisComponent', () => {
  let component: ExternPropertisComponent;
  let fixture: ComponentFixture<ExternPropertisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternPropertisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternPropertisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
