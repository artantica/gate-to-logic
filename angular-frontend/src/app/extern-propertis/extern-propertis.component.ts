import { Project } from './../project';
import { ApiService } from './../api.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-extern-propertis',
  templateUrl: './extern-propertis.component.html',
  styleUrls: ['./extern-propertis.component.css']
})
export class ExternPropertisComponent implements OnInit {

  form = new FormGroup({
    project: new FormControl('',Validators.required)
  })

  projects : Project[] = []
  inputs : {id:number,name:string}[] = []
  outputs : {id:number,name:string}[] = []
  system = undefined

  constructor(private api : ApiService, @Inject(MAT_DIALOG_DATA) public data: {},public dialogRef: MatDialogRef<ExternPropertisComponent>) { }

  ngOnInit() {
    this.api.getProjects().subscribe(response=>{
      this.projects = response
    })
  }

  choose($event) {
    console.log($event)
    let project = this.projects.filter(e=>e.id==$event.value)[0]
    console.log(project)
    this.system = JSON.parse(project.representation)
    this.inputs = this.system.elements.filter(e=>{return e&&e.type==15}).map(e=>{ return {id:e.id,name:e.name}})
    this.outputs = this.system.elements.filter(e=>{return e&&e.type==16}).map(e=>{ return {id:e.id,name:e.name}})
  }

  indrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.inputs, event.previousIndex, event.currentIndex);
  }
  outdrop(event: CdkDragDrop<string[]>) {
    console.log(event,this.outputs)
    moveItemInArray(this.outputs, event.previousIndex, event.currentIndex);
  }

  make() {
    this.dialogRef.close({inputs:this.inputs.map(e=>{return e.id}),outputs:this.outputs.map(e=>{return e.id}),system:this.system})
  }

}
