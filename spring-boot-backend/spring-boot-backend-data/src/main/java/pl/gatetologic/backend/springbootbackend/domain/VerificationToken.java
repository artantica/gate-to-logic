package pl.gatetologic.backend.springbootbackend.domain;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

/**
 *  Klasa reprezentująca token weryfikacji.
 *  Posiada on token w postaci napisu o długości 36 i date wygaśnięcia, która ustatwiana jest na 24 godziny po dacie utworzenia obiektu.
 *
 *  Pola tej klasy posiadają odpowiednie walidatory z biblioteki {@link javax.validation.constraints}.
 *  Także używa Project Lombok do generowania odpowiednich fragmentów kodu:
 *  {@link lombok.Getter}, {@link lombok.NoArgsConstructor},{@link lombok.Builder},{@link lombok.AllArgsConstructor},
 *  {@link lombok.EqualsAndHashCode}, {@link lombok.Setter}
 *  W bazie danych jest zapisywany jako objekt w odpowiedniej kolekcji mongo.
 *
 * @see lombok
 * @see pl.gatetologic.backend.springbootbackend.domain.Project
 * @see java.io.Serializable
 * @see org.springframework.data.mongodb.core.mapping.Document
 */
@Getter
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class VerificationToken implements Serializable {
    @Size(min = 36, max = 36)
    private String token;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date expiryDate;

    public VerificationToken() {
        this.token = null;
        this.expiryDate = null;
    }

    /**
     * Konstruktor, który ustawia datę wygaśnięcia tokena na 24 godziny po dacie utworzenia,
     * oraz generuje token.
     * @param now
     */
    public VerificationToken(Instant now) {
//        Instant after = now.plus(Duration.ofSeconds(1));
        Instant after = now.plus(Duration.ofDays(1));
        this.expiryDate = Date.from(after);
        this.token = UUID.randomUUID().toString();
    }
}
