package pl.gatetologic.backend.springbootbackend.domain;

import lombok.*;

import javax.validation.constraints.*;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 *  Klasa reprezentująca użytkownika aplikacji.
 *  Posiada dane do logowania i bezpieczeństwa aplikacji.
 *  Także posiada listę swoich projektów.
 *  Pola tej klasy posiadają odpowiednie walidatory z biblioteki {@link javax.validation.constraints}.
 *  Także używa Project Lombok do generowania odpowiednich fragmentów kodu:
 *  {@link lombok.Getter}, {@link lombok.NoArgsConstructor},{@link lombok.Builder},{@link lombok.AllArgsConstructor},
 *  {@link lombok.EqualsAndHashCode}, {@link lombok.Setter}
 *  W bazie danych jest zapisywany jako objekt w odpowiedniej kolekcji mongo.
 *
 * @see lombok
 * @see pl.gatetologic.backend.springbootbackend.domain.Project
 * @see java.io.Serializable
 * @see org.springframework.data.mongodb.core.mapping.Document
 */
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode
@Document
public class User implements Serializable {

    /**
     * Identyfikator objektu automatycznie generowany przed bazę danych.
     * W bazie jako ObjectId.
     *
     * @see org.springframework.data.annotation.Id
     */
    @Id
    private String id;
    /**
     * Login użytkownika.
     * Nie może być ani znakiem pustym ani null'em. Ma ograniczenie od 4 do 30 znaków.
     * Login jest unikalny dla każdego użytkownika.
     *
     * @see javax.validation.constraints.NotBlank
     * @see javax.validation.constraints.Size
     */
    @NotBlank
    @Size(min = 4, max = 30, message = "Login need to have between 4 and 30 characters")
    private String login;
    /**
     * Hasło uzytkownika.
     * Nie może być null'em. Ma ograniczenie na przynajmniej 8 znaków.
     *
     * @see javax.validation.constraints.NotNull
     * @see javax.validation.constraints.Size
     */
    @NotNull
    @Size(min = 8, message = "Password need to have minimum 8 characters")
    private String password;
    /**
     * Email użytkownika.
     * Nie może być ani znakiem pustym ani null'em. Musi być formatu email.
     * Na jeden email może być zarejestrowany tylko 1 użytkownik.
     *
     * @see javax.validation.constraints.NotBlank
     * @see javax.validation.constraints.Email
     */
    @NotBlank
    @Email
    private String email;
    /**
     *  Token użytkownika służący do resetowania hałsa.
     *  Po prośbie uzytkownika o zmiane hasła zostaje mu nadany unikalny token.
     *  Czas ważności tokenu to 24h.
     *  Po wykorzystaniu tokena do zmiany hasła zostanie on zresetowany.
     *  Ma ograniczenie na dokładnie 36 znaków.
     */

    @Setter
    private VerificationToken passwordResetToken = new VerificationToken();
    /**
     *  Token weryfikacyjny mail użytkownika, czas ważności to 24h.
     */
    @Setter
    private VerificationToken emailVerifyToken = new VerificationToken();
    /**
     *  Weryfikacja mailu użytkownika.
     *  Podczas rejestracji użytkownik musi potwierdzić rejestrację emailem, wtedy verified zmienia wartość na true.
     */
    private Boolean verified = false;
    /**
     * Lista projektów użytkownika.
     * W bazie danych są zapisywane jako lista ObjectId.
     *
     * @see org.springframework.data.mongodb.core.mapping.DBRef
     * @see pl.gatetologic.backend.springbootbackend.domain.Project
     */
    @DBRef
    private List<Project> projects = new ArrayList<>();
}
