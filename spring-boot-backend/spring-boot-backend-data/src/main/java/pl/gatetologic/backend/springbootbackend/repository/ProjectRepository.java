package pl.gatetologic.backend.springbootbackend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.gatetologic.backend.springbootbackend.domain.Project;

/**
 * Interfejs dziedziczący po MongoRepository odpowiedzialny za komunikację z bazą danych w kontekście klasy Project.
 *  Instancja interfejsu jest tworzona za pomocą framework'a Spring Boot oraz adnotacji @Repository.
 *
 *  @see org.springframework.stereotype.Repository
 *  @see org.springframework.data.mongodb.repository.MongoRepository
 *  @see pl.gatetologic.backend.springbootbackend.domain.Project
 */
@Repository
public interface ProjectRepository extends MongoRepository<Project, String> {

}
