package pl.gatetologic.backend.springbootbackend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.gatetologic.backend.springbootbackend.domain.User;
import pl.gatetologic.backend.springbootbackend.domain.VerificationToken;

import java.util.Date;
import java.util.Optional;

/**
 * Interfejs dziedziczący po MongoRepository odpowiedzialny za komunikację z bazą danych w kontekście klasy User.
 *  Instancja interfejsu jest tworzona za pomocą framework'a Spring Boot oraz adnotacji @Repository.
 *
 * @see org.springframework.data.mongodb.repository.MongoRepository
 * @see pl.gatetologic.backend.springbootbackend.domain.User
 * @see org.springframework.stereotype.Repository
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {
    /**
     *  Metoda pobierająca z bazy danych obiekt User o podanym loginie.
     *
     * @param login String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego loginu.
     * @return Optional Zwracany jest obiekt Optional typu User. Optional jest pusty w przypadku
     *  kiedy w bazie nie znajduje się User o danym loginie. W przeciwnym wypadku zwracany jest obiekt znalezionego
     *  Usera opakowanego w obiekt Optional.
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.User
     * @see java.util.Optional
     */
    Optional<User> findByLogin(String login);
    /**
     *  Metoda pobierająca z bazy danych obiekt User o podanym mailu.
     *
     * @param email String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego maila.
     * @return Optional Zwracany jest obiekt Optional typu User. Optional jest pusty w przypadku
     *  kiedy w bazie nie znajduje się User o danym mailu. W przeciwnym wypadku zwracany jest obiekt znalezionego
     *  Usera opakowanego w obiekt Optional.
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.User
     * @see java.util.Optional
     */
    Optional<User> findByEmail(String email);
    /**
     *  Metoda pobierająca z bazy danych obiekt User o podanym tokenie zmiany hasła.
     *
     * @param resetPasswordToken Token według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu zmiany hasła.
     * @return Optional Zwracany jest obiekt Optional typu User. Optional jest pusty w przypadku
     *  kiedy w bazie nie znajduje się User o podanym tokenie. W przeciwnym wypadku zwracany jest obiekt znalezionego
     *  Usera opakowanego w obiekt Optional.
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.User
     * @see java.util.Optional
     */
    Optional<User> findByPasswordResetToken_Token(String resetPasswordToken);
    /**
     *  Metoda pobierająca z bazy danych obiekt User o podanym tokenie weryfikacji maila.
     *
     * @param emailVerifyToken Token według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu weryfikacji.
     * @return Optional Zwracany jest obiekt Optional typu User. Optional jest pusty w przypadku
     *  kiedy w bazie nie znajduje się User o podanym tokenie. W przeciwnym wypadku zwracany jest obiekt znalezionego
     *  Usera opakowanego w obiekt Optional.
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.User
     * @see java.util.Optional
     */
    Optional<User> findByEmailVerifyToken_Token(String emailVerifyToken);
    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym loginie.
     *
     * @param login String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego loginu.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym loginie.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    Boolean existsByLogin(String login);
    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym mailu.
     *
     * @param email String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego maila.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym mailu.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    Boolean existsByEmail(String email);
    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym tokenie zmiany hasła.
     *
     * @param resetPasswordToken String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu zmiany hasła.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym tokenie.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    Boolean existsByPasswordResetToken_Token(String resetPasswordToken);
    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym tokenie zmiany hasła.
     *
     * @param emailVerifyToken String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu tokenu weryfikacji maila.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym tokenie.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    Boolean existsByEmailVerifyToken_Token(String emailVerifyToken);
}
