package pl.gatetologic.backend.springbootbackend.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;



/**
 *  Klasa reprezentująca projekt w aplikacji.
 *  Zawiera on odpowiednie informacje identyfikujące dany projekt.
 *  Id oraz Name są odpowiedzialne za identyfikację.
 *  Pole description jest odpowiedzialne za przechowywanie opisu projektu.
 *  Representation jest reprezentacją układu logicznego zawartego w danym projekcie.
 *  Pola tej klasy posiadają odpowiednie walidatory z biblioteki {@link javax.validation.constraints}.
 *  Także używa Project Lombok do generowania odpowiednich fragmentów kodu:
 *  {@link lombok.Getter}, {@link lombok.NoArgsConstructor},{@link lombok.Builder},{@link lombok.AllArgsConstructor},
 *  {@link lombok.EqualsAndHashCode},
 *  W bazie danych jest zapisywany jako objekt w odpowiedniej kolekcji mongo.
 *
 * @see lombok
 * @see java.io.Serializable
 * @see org.springframework.data.mongodb.core.mapping.Document
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Document
public class Project implements Serializable {

    /**
     * Identyfikator objektu automatycznie generowany przed bazę danych.
     * W bazie jako ObjectId.
     *
     * @see org.springframework.data.annotation.Id
     */
    @Id
    private String id;
    /**
     * Nazwa projektu.
     * Nie może być ani znakiem pustym ani null'em. Ma ograniczenie na 40 znaków.
     *
     * @see javax.validation.constraints.NotBlank
     * @see javax.validation.constraints.Size
     */
    @NotBlank
    @Size(max = 40)
    private String name;

    /**
     * Opis projektu.
     * Nie ma ograniczeń jeśli chodzi o znaki.
     */
    private String description;

    /**
     * Reprezentacja układu projektu.
     * Jeden projekt może posiadać tylko jeden układ.
     */
    private String representation;
}
