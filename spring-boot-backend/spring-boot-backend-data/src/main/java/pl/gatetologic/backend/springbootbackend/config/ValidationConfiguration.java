package pl.gatetologic.backend.springbootbackend.config;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 *  Klasa konfiguracyjna Springa.
 *  Posiada generowanie Beanów dla walidatorów MongoDB.
 *
 * @see org.springframework.context.annotation.Configuration
 */
@Configuration
public class ValidationConfiguration {
    /**
     *
     * @return Bean validator listener'a dla MongoDB
     */
    @Bean
    public ValidatingMongoEventListener validatingMongoEventListener() {
        return new ValidatingMongoEventListener(validator());
    }

    /**
     *
     * @return Bean Factory Bean dla walidatorów.
     */
    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }
}
