package pl.gatetologic.backend.springbootbackend.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.gatetologic.backend.springbootbackend.domain.Project;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProjectMapperTest {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String REPRESENTATION = "representation";

    @Mock
    ProjectMapper mapper;

    private Project project;

    @BeforeEach
    void setUp() {
        project = Project.builder()
                .id(ID)
                .name(NAME)
                .description(DESCRIPTION)
                .representation(REPRESENTATION)
                .build();
    }

//    @Test
//    void toProjectDtoWithoutSystem() {
//        when(mapper.toProjectDtoWithoutSystem(any(Project.class))).thenCallRealMethod();
//
//        ProjectDto returnedDto = mapper.toProjectDtoWithoutSystem(project);
//
//        assertNotNull(returnedDto);
//        assertEquals(ID,returnedDto.getId());
//        assertEquals(NAME, returnedDto.getName());
//        assertEquals(DESCRIPTION,returnedDto.getDescription());
//        assertNull(returnedDto.getRepresentation());
//    }


//    @Test
//    void toProjectDtoList() {
//        when(mapper.toProjectDtoList(anyList())).thenCallRealMethod();
//        when(mapper.toProjectDtoWithoutSystem(any(Project.class))).thenCallRealMethod();
//
//        Project project1 = Project.builder().id(ID + "1").build();
//        List<Project> projects = List.of(project, project1);
//
//        ProjectDto expectedDto0 = mapper.toProjectDtoWithoutSystem(project);
//        ProjectDto expectedDto1 = mapper.toProjectDtoWithoutSystem(project1);
//
//        List<ProjectDto> returnedDtos = mapper.toProjectDtoList(projects);
//
//        assertNotNull(returnedDtos);
//        assertEquals(2, returnedDtos.size());
//        assertEquals(expectedDto0, returnedDtos.get(0));
//        assertEquals(expectedDto1, returnedDtos.get(1));
//    }


}