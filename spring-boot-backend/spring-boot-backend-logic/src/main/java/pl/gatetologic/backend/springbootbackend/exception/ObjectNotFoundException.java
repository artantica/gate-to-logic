package pl.gatetologic.backend.springbootbackend.exception;

/**
 * Wyjatek rzucany w momencie odwołania się do nieistniejącego obiektu w bazie danych.
 *
 * @see RuntimeException
 */
public class ObjectNotFoundException extends RuntimeException {

    /**
     * Konstruktor używający konstruktora klasy nadrzędnej.
     *
     * @param message Wiadomość wyjątku.
     */
    public ObjectNotFoundException(String message) {
        super(message);
    }
}
