package pl.gatetologic.backend.springbootbackend.dto;

import lombok.*;

/**
 *  Klasa Project Data Transfer Object. Kopia obiektu Project.
 *  Instancje klasy DTO są używane do przesyłania danych i operowania na nich podczas modyfikowania,
 *  aby nie ruszać instancji bezpośrednio z bazy danych. Pola klasy DTO są to kopie pól klasy bazowej.
 *
 * @see lombok.Data
 * @see lombok.Builder
 * @see lombok.NoArgsConstructor
 * @see lombok.AllArgsConstructor
 * @see pl.gatetologic.backend.springbootbackend.domain.Project
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDto {
    private String id;
    private String name;
    private String description;
    private String representation;
}
