package pl.gatetologic.backend.springbootbackend.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import pl.gatetologic.backend.springbootbackend.domain.VerificationToken;
import pl.gatetologic.backend.springbootbackend.dto.VerificationTokenDto;

/**
 * Interfejs mapująca obiekty klas VerificationTokenMapper oraz VerificationTokenMapperDto.
 * Spring Boot wygeneruje implementację interfejsu jako Bean.
 * Klasa ta jest odpowiedzialna za mapowanie dwóch klas pomiędzy sobą.
 *
 * @see org.mapstruct.Mapper
 * @see pl.gatetologic.backend.springbootbackend.domain.VerificationToken
 * @see pl.gatetologic.backend.springbootbackend.dto.VerificationTokenDto
 */
@Mapper(componentModel = "spring",injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface VerificationTokenMapper {
    /**
     *  Mapuje wartości pól klasy VerificationToken do pól klasy VerificationTokenDto, tworząc nowy obiekt.
     *
     * @param system VerificationToken do zmapowania na VerificationTokenDto
     * @return zwraca zmapowaną instancję klasy VerificationTokenDto
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.VerificationToken
     * @see pl.gatetologic.backend.springbootbackend.dto.VerificationTokenDto
     */
    VerificationTokenDto toVerificationTokenDto(VerificationToken system);

    /**
     * Mapuje wartości pól klasy VerificationTokenDto do pól klasy VerificationToken, tworząc nowy obiekt.
     *
     *  @param VerificationTokenDto VerificationTokenDto do zmapowania na VerificationToken
     *  @return zwraca zmapowaną instancję klasy VerificationToken
     *
     *  @see pl.gatetologic.backend.springbootbackend.domain.VerificationToken
     *  @see pl.gatetologic.backend.springbootbackend.dto.VerificationTokenDto
     */
    VerificationToken toVerificationToken(VerificationTokenDto VerificationTokenDto);
}

