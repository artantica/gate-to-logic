package pl.gatetologic.backend.springbootbackend.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.gatetologic.backend.springbootbackend.domain.User;
import pl.gatetologic.backend.springbootbackend.domain.VerificationToken;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.mapper.UserMapper;
import pl.gatetologic.backend.springbootbackend.repository.UserRepository;
import pl.gatetologic.backend.springbootbackend.service.declaration.UserService;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *  Klasa serwisowa odpowiedzalna za operacje na obiektach typu User oraz UserDTO.
 *  Implementuje interfejs UserService.
 *  Używa on instancji UserMapper do mapowania pomiędzy dwoma typami klasy User
 *  oraz używa ProjectRepository do komunikacji z bazą danych.
 *  Używa adnotacji @Service dzięki czemu jest obsługiwany przez framework Spring Boot
 *
 * @see pl.gatetologic.backend.springbootbackend.service.declaration.UserService
 * @see pl.gatetologic.backend.springbootbackend.repository.UserRepository
 * @see pl.gatetologic.backend.springbootbackend.mapper.UserMapper
 * @see pl.gatetologic.backend.springbootbackend.domain.User
 * @see pl.gatetologic.backend.springbootbackend.dto.UserDto
 * @see org.springframework.stereotype.Service
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper mapper;
    private final ServiceProxy proxy;

    /**
     * Konstruktor używany przez SpringBoot.
     * Dzięki niemu jest użyte wstrzykiwanie zależności.
     *
     * @param userRepository Bean repozytorium
     * @param proxy Bean komunikatora pomiędzy serwerami
     * @param mapper Bean mappera
     */
    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserMapper mapper, ServiceProxy proxy) {
        this.userRepository = userRepository;
        this.mapper = mapper;
        this.proxy = proxy;
    }

    /**
     * Metoda do ustawienia Beana UserService w ServiceProxy.
     * Niweluje zapętlenie zależności
     *
     * @see pl.gatetologic.backend.springbootbackend.service.implementation.ServiceProxy
     */
    @PostConstruct
    public void postConstruct(){
        this.proxy.setUserService(this);
    }

    /**
     * Metoda znajdująca w bazie danych obiekt typu User z podanym identyfikatorem id.
     * W przypadku nie znalezienia pożadanego obiektu zostanie wyrzucony RuntimeException typu ObjectNotFoundException.
     * Metoda wykorzystuje mapper oraz repositorium do pobrania obiektu z bazy a następnie zmapowaniu na obiekt DTO.
     *
     * @param id Dany identyfikator względem którego szukany jest obiekt w bazie danych.
     * @return Zwraca znaleziony obiekt w postaci DTO.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.UserMapper
     * @see pl.gatetologic.backend.springbootbackend.repository.UserRepository
     * @see pl.gatetologic.backend.springbootbackend.exception.ObjectNotFoundException
     */
    @Override
    public UserDto findById(String id) {
        Optional<User> optionalUser = userRepository.findById(id);
        return optionalUser
                .map(mapper::toUserDto)
                .orElse(null);
    }

    /**
     * Metoda znajdująca w bazie danych obiekt typu User z podanym loginem.
     * Metoda wykorzystuje mapper oraz repositorium do pobrania obiektu z bazy, a następnie zmapowaniu na obiekt DTO.
     *
     * @param login Dany login, względem którego szukany jest obiekt w bazie danych.
     * @return Zwraca znaleziony obiekt w postaci DTO lub w przypadku nieznalezienia zwraca obiekt null.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.UserMapper
     * @see pl.gatetologic.backend.springbootbackend.repository.UserRepository
     * @see pl.gatetologic.backend.springbootbackend.exception.ObjectNotFoundException
     */
    @Override
    public UserDto findByLogin(String login) {
        Optional<User> optionalUser = userRepository.findByLogin(login);
        return optionalUser
                .map(mapper::toUserDto)
                .orElse(null);
    }

    /**
     * Metoda znajdująca w bazie danych obiekt typu User z podanym mailem.
     * Metoda wykorzystuje mapper oraz repositorium do pobrania obiektu z bazy, a następnie zmapowaniu na obiekt DTO.
     *
     * @param email Dany mail, względem którego szukany jest obiekt w bazie danych.
     * @return Zwraca znaleziony obiekt w postaci DTO lub w przypadku nieznalezienia zwraca obiekt null.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.UserMapper
     * @see pl.gatetologic.backend.springbootbackend.repository.UserRepository
     * @see pl.gatetologic.backend.springbootbackend.exception.ObjectNotFoundException
     */
    @Override
    public UserDto findUserByEmail(final String email) {
        Optional<User> optionalUser = userRepository.findByEmail(email);
        return optionalUser
                .map(mapper::toUserDto)
                .orElse(null);
    }

    /**
     * Metoda znajdująca w bazie danych obiekt typu User z podanym tokenem resetowania hasła.
     * Metoda wykorzystuje mapper oraz repositorium do pobrania obiektu z bazy, a następnie zmapowaniu na obiekt DTO.
     *
     * @param resetPasswordToken Dany token, względem którego szukany jest obiekt w bazie danych.
     * @return Zwraca znaleziony obiekt w postaci DTO lub w przypadku nieznalezienia zwraca obiekt null.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.UserMapper
     * @see pl.gatetologic.backend.springbootbackend.repository.UserRepository
     * @see pl.gatetologic.backend.springbootbackend.exception.ObjectNotFoundException
     */
    @Override
    public UserDto findUserByResetPasswordToken(String resetPasswordToken) {
        Optional<User> optionalUser = userRepository.findByPasswordResetToken_Token(resetPasswordToken);
//        if (isTokenDateExpired(this.findUserByResetPasswordToken(resetPasswordToken).getEmailVerifyToken().getExpiryDate())) return null;
        return optionalUser
                .map(mapper::toUserDto)
                .orElse(null);
    }

    /**
     *  Metoda szukająca użytkownika względem podanego tokenu weryfikacji maila.
     *
     * @param emailVerifyToken Token według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu weryfikacji.
     * @return UserDto Zwracany jest obiekt UserDto. Jeśli w bazie nie znajduje się User o podanym tokenie
     * zwraca null, w przeciwnym wypadku zwracany jest obiekt znalezionego Usera opakowanego w obiekt UserDto.
     * Metoda wykorzystuje mapper oraz repositorium do pobrania obiektu z bazy, a następnie zmapowaniu na obiekt DTO.
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.User
     */
    @Override
    public UserDto findUserByEmailVerifyToken(String emailVerifyToken) {
        Optional<User> optionalUser = userRepository.findByEmailVerifyToken_Token(emailVerifyToken);
        return optionalUser
                .map(mapper::toUserDto)
                .orElse(null);
    }

    /**
     *  Metoda zapisuje podany obiekt typu User w bazie oraz zwraca reprezentacje DTO zapisanego obiektu.
     *  Używane jest odpowiednie repozytorium do zapisania w bazie, nastepnie odpowiedni mapper mapuje zapisany obiekt
     *  na postać DTO, któ©a jest ostatecznie zwracana.
     *
     * @param object obiekt do zapisania.
     * @return Zwraca reprezentacje DTO zapisanego obiektu.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.UserMapper
     * @see pl.gatetologic.backend.springbootbackend.repository.UserRepository
     */
    @Override
    public UserDto save(User object) {
        return mapper.toUserDto(userRepository.save(object));
    }

    /**
     *  Metoda zapisuje podany obiekt typu UserDTO w bazie oraz zwraca reprezentacje DTO zapisanego obiektu.
     *  Używany jest mapper.
     *
     * @param object obiekt do zapisania.
     * @return Zwraca reprezentacje DTO zapisanego obiektu.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.UserMapper
     */
    @Override
    public UserDto saveDTO(UserDto object) {
        return this.save(mapper.toUser(object));
    }

    /**
     * Metoda usuwa z bazy danych obiekt typu User o identyfikatorze danego s.
     * Wywoływana jest metoda odpowiedniego repozytorium do usunięcia z bazy konkretnego zasobu.
     *
     * @param id Identyfikator obiektu do usunięcia.
     *
     * @see pl.gatetologic.backend.springbootbackend.repository.UserRepository
     */
    @Override
    public void deleteById(String id) {
        userRepository.deleteById(id);
    }

    /**
     * Metoda zwracająca wszystkich użytkowników.
     *
     * @return zwraca wszystkich użytkowników jako listę obiektów UserDto
     */
    @Override
    public List<UserDto> getAllUsers() {
        return null;
    }

    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym loginie.
     *
     * @param login String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego loginu.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym loginie.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    @Override
    public Boolean existsByLogin(String login) {
        return userRepository.existsByLogin(login);
    }

    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym mailu.
     *
     * @param email String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego maila.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym mailu.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    @Override
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym tokenie zmiany hasła.
     *
     * @param resetPasswordToken String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu zmiany hasła.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym tokenie.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    @Override
    public Boolean existsByPasswordResetToken(String resetPasswordToken) {
        if (isTokenDateExpired(this.findUserByResetPasswordToken(resetPasswordToken).getEmailVerifyToken().getExpiryDate())) return false;
        return userRepository.existsByPasswordResetToken_Token(resetPasswordToken);
    }

    /**
     * Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym tokenie zmiany hasła.
     *
     * @param emailVerifyToken String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu weryfikacji maila.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym tokenie.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    @Override
    public Boolean existsByEmailVerifyToken(String emailVerifyToken) {
        if (!userRepository.existsByEmailVerifyToken_Token(emailVerifyToken) || isTokenDateExpired(this.findUserByEmailVerifyToken(emailVerifyToken).getEmailVerifyToken().getExpiryDate())) return false;
        return userRepository.existsByEmailVerifyToken_Token(emailVerifyToken);
    }

    /**
     *  Metoda zmieniająca token zmiany hasła dla podanego użytkownika.
     *
     * @param passwordResetToken VerificationToken Nowy token zmiany hasła.
     * @return UserDto zwraca użytkownika ze zmienionym tokenem jako UserDto
     *
     */
    @Override
    public UserDto setUserPasswordResetToken(UserDto userDto, VerificationToken passwordResetToken) {
        userDto.setPasswordResetToken(passwordResetToken);
        return saveDTO(userDto);
    }

    /**
     *  Metoda zmieniająca token weryfikacji maila dla podanego użytkownika.
     *
     * @param emailVerifyToken VerificationToken - nowy token weryfikacji maila.
     * @return UserDto zwraca użytkownika ze zmienionym tokenem jako UserDto.
     *
     */
    @Override
    public UserDto setUserEmailVerifyToken(UserDto userDto, VerificationToken emailVerifyToken) {
        userDto.setEmailVerifyToken(emailVerifyToken);
        return saveDTO(userDto);
    }

    /**
     *  Metoda zmieniająca hasło dla podanego użytkownika.
     *
     * @param password String Nowe hasło.
     * @return UserDto zwraca użytkownika ze zmienionym hasłem jako UserDto.
     *
     */
    @Override
    public UserDto changeUserPassword(UserDto userDto, final String password) {
        userDto.setPassword(password);
        return saveDTO(userDto);
    }

    /**
     * Metoda ustawiająca pole weryfikacji mailowej jako true.
     * @param userDto Użytkownik, którego mail został zweryfikowany
     *
     * @return UserDto zwraca użytkownika ze zweryfikowanym mailem
     */
    @Override
    public UserDto setUserVerified(UserDto userDto) {
        userDto.setVerified(true);
        return saveDTO(userDto);
    }

    /**
     * Metoda sprawdzająca, czy podany użytkownik ma zweryfikowany mail.
     * @param userDto Sprawdzany użytkownik
     * @return Zwraca true, jeśli użytkownik ma zweryfikowany mail, w przeciwnym wypadku zwraca false.
     */
    @Override
    public Boolean isUserVerified(UserDto userDto) {
        return userDto.getVerified();
    }

    /**
     * Prywatna metoda, do sprawdzenia czy podana data już minęła.
     * @param tokenDate Podana data wygaśnięcia tokena
     * @return true jeśli data minęła, false w przeciwnym wypadku.
     */
    private Boolean isTokenDateExpired(Date tokenDate) {
        Date current = new Date();
        return !tokenDate.before(current);
    }
}
