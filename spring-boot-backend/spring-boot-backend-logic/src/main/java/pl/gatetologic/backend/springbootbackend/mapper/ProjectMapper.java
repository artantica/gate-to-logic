package pl.gatetologic.backend.springbootbackend.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import pl.gatetologic.backend.springbootbackend.domain.User;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;
import pl.gatetologic.backend.springbootbackend.domain.Project;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;

import java.util.List;
import java.util.stream.Collectors;



/**
 * Interfejs mapująca obiekty klas Project oraz ProjectDto.
 * Spring Boot wygeneruje implementację interfejsu jako Bean.
 * Klasa ta jest odpowiedzialna za mapowanie dwóch klas pomiędzy sobą.
 *
 * @see org.mapstruct.Mapper
 * @see pl.gatetologic.backend.springbootbackend.domain.Project
 * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
 */
@Mapper(componentModel = "spring",injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ProjectMapper {

    /**
     *  Mapuje wartości pól klasy Project do pól klasy ProjectDto, tworząc nowy obiekt.
     *
     * @param project Project do zmapowania na ProjectDto
     * @return zwraca zmapowaną instancję klasy ProjectDto
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.Project
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     */
    ProjectDto toProjectDto(Project project);
    /**
     * Mapuje wartości pól klasy ProjectDto do pól klasy Project, tworząc nowy obiekt.
     *
     *  @param projectDto ProjectDto do zmapowania na Project
     *  @return zwraca zmapowaną instancję klasy Project
     *
     *  @see pl.gatetologic.backend.springbootbackend.domain.Project
     *  @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     */
    Project toProject(ProjectDto projectDto);

    /**
     * Metoda mapująca listę obiektów klasy Project na liste obiektów klasy ProjectDto.
     *
     * @param projects lista projektów (klasy Project) do zmapowania.
     * @return zwraca lstę obiektów klasy ProjectDto.
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.Project
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     */
    default List<ProjectDto> toProjectDtoList(List<Project> projects){
        return projects.stream()
                .map(this::toProjectDto)
                .collect(Collectors.toList());
    }

//    /**
//     * Mapuje wartości pól klasy Project do pól klasy ProjectDto, tworząc nowy obiekt.
//     * Natomiast wartość pola system w zwracanym obiekcie ma wartość null.
//     *
//     * @param project instancja klasy Project do zmapowania
//     * @return zwraca instancje klasy ProjectDto bez zmapowanego pola system. System zwracanego obiektu jest nullem.
//     *
//     * @see pl.gatetologic.backend.springbootbackend.domain.Project
//     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
//     */
//    default ProjectDto toProjectDtoWithoutSystem(Project project){
//        ProjectDto.ProjectDtoBuilder builder = ProjectDto.builder();
//        builder.name(project.getName());
//        builder.id(project.getId());
//        builder.representation(project.getRepresentation());
//        builder.description(project.getDescription());
//        return builder.build();
//    }
}
