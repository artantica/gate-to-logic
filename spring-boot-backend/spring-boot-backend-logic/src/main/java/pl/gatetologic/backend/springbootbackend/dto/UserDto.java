package pl.gatetologic.backend.springbootbackend.dto;

import lombok.*;
import pl.gatetologic.backend.springbootbackend.domain.User;
import pl.gatetologic.backend.springbootbackend.domain.VerificationToken;

import java.util.List;
import java.util.Optional;

/**
 *  Klasa User Data Transfer Object. Kopia obiektu User.
 *  Instancje klasy DTO są używane do przesyłania danych i operowania na nich podczas modyfikowania,
 *  aby nie ruszać instancji bezpośrednio z bazy danych. Pola klasy DTO są to kopie pól klasy bazowej.
 *
 * @see lombok.Data
 * @see lombok.Builder
 * @see lombok.Getter
 * @see lombok.Setter
 * @see lombok.NoArgsConstructor
 * @see lombok.AllArgsConstructor
 * @see pl.gatetologic.backend.springbootbackend.domain.User
 * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
 */
@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String id;
    private String login;
    private String password;
    private String email;
    private VerificationToken passwordResetToken;
    private VerificationToken emailVerifyToken;
    private Boolean verified;
    private List<ProjectDto> projects;
}
