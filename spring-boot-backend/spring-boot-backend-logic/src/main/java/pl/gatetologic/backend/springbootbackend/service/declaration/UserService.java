package pl.gatetologic.backend.springbootbackend.service.declaration;

import pl.gatetologic.backend.springbootbackend.domain.User;
import pl.gatetologic.backend.springbootbackend.domain.VerificationToken;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import java.util.List;

/**
 * Serwis dziedziczacy po BaseService.
 * UserService jest odpowiedzialny za operowaniu głównie na obiektach typu User oraz UserDto.
 *
 * @see pl.gatetologic.backend.springbootbackend.service.declaration.BaseService
 * @see pl.gatetologic.backend.springbootbackend.service.implementation.UserServiceImpl
 * @see pl.gatetologic.backend.springbootbackend.domain.User
 * @see pl.gatetologic.backend.springbootbackend.dto.UserDto
 */
public interface UserService extends BaseService<User, UserDto, String> {
    /**
     * Metoda szukająca użytkownika względem podanego loginu.
     *
     * @param login Login względem jest szukany użytkownik.
     * @return zwraca znalezionego użytkownika jako UserDto
     */
    UserDto findByLogin(String login);
    /**
     * Metoda szukająca użytkownika względem podanego maila.
     *
     * @param email Mail względem jest szukany użytkownik.
     * @return zwraca znalezionego użytkownika jako UserDto
     */
    UserDto findUserByEmail(final String email);
    /**
     * Metoda szukająca użytkownika względem podanego tokenu resetowania hasła.
     *
     * @param resetPasswordToken Token według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu zmiany hasła.
     * @return UserDto Zwracany jest obiekt UserDto. Jeśli w bazie nie znajduje się User o podanym tokenie
     * zwraca null, w przeciwnym wypadku zwracany jest obiekt znalezionego Usera opakowanego w obiekt UserDto.
     * @return zwraca znalezionego użytkownika jako UserDto
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.VerificationToken
     */
    UserDto findUserByResetPasswordToken(String resetPasswordToken);

    /**
     *  Metoda szukająca użytkownika względem podanego tokenu weryfikacji maila.
     *
     * @param emailVerifyToken Token według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu weryfikacji.
     * @return UserDto Zwracany jest obiekt UserDto. Jeśli w bazie nie znajduje się User o podanym tokenie
     * zwraca null, w przeciwnym wypadku zwracany jest obiekt znalezionego Usera opakowanego w obiekt UserDto.
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.User
     */
    UserDto findUserByEmailVerifyToken(String emailVerifyToken);

    /**
     * Metoda zwracająca wszystkich użytkowników.
     *
     * @return zwraca wszystkich użytkowników jako listę obiektów UserDto
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.VerificationToken
     */
    List<UserDto> getAllUsers();

    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym loginie.
     *
     * @param login String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego loginu.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym loginie.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    Boolean existsByLogin(String login);
    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym mailu.
     *
     * @param email String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego maila.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym mailu.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    Boolean existsByEmail(String email);
    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym tokenie zmiany hasła.
     *
     * @param passwordResetToken String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu zmiany hasła.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym tokenie.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     */
    Boolean existsByPasswordResetToken(String passwordResetToken);
    /**
     *  Metoda sprawdzająca, czy w bazie znajduje się użytkownik o podanym tokenie zmiany hasła.
     *
     * @param emailVerifyToken String według którego odnajdywany jest w bazie danych obiekt na podstawie pasującego tokenu tokenu weryfikacji maila.
     * @return Boolean Zwracana jest wartość false w przypadku kiedy w bazie nie znajduje się User o podanym tokenie.
     * W przeciwnym wypadku zwracana jest wartośc true.
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.VerificationToken
     */
    Boolean existsByEmailVerifyToken(String emailVerifyToken);

    /**
     *  Metoda zmieniająca token zmiany hasła dla podanego użytkownika.
     *
     * @param passwordResetToken VerificationToken Nowy token zmiany hasła.
     * @return UserDto zwraca użytkownika ze zmienionym tokenem jako UserDto
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.VerificationToken
     */
    UserDto setUserPasswordResetToken(UserDto userDto, VerificationToken passwordResetToken);

    /**
     *  Metoda zmieniająca token weryfikacja maila dla podanego użytkownika.
     *
     * @param emailVerifyToken VerificationToken Nowy token weryfikacji maila.
     * @return UserDto zwraca użytkownika ze zmienionym tokenem jako UserDto
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.VerificationToken
     */
    UserDto setUserEmailVerifyToken(UserDto userDto, VerificationToken emailVerifyToken);


    /**
     * Metoda zmieniająca hasło dla podanego użytkownika.
     *
     * @param password String Nowe hasło.
     * @return UserDto zwraca użytkownika ze zmienionym hasłem jako UserDto
     *
     */
    UserDto changeUserPassword(UserDto userDto, final String password);

    /**
     * Metoda ustawiająca true dla pola weryfikacji maila.
     *
     * @return UserDto zwraca użytkownika ze zweryfikowanym mailem.
     *
     */
    UserDto setUserVerified(UserDto userDto);

    /**
     * Metoda sprawdzająca, czy użytkownik zweryfikował maila.
     *
     * @return UserDto zwraca użytkownika ze zweryfikowanym mailem.
     *
     */
    Boolean isUserVerified(UserDto userDto);
}
