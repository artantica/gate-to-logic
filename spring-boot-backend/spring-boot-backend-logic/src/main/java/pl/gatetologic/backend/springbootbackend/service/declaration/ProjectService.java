package pl.gatetologic.backend.springbootbackend.service.declaration;

import pl.gatetologic.backend.springbootbackend.domain.Project;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;

import java.util.List;

/**
 * Serwis dziedziczacy po BaseService.
 * ProjectService jest odpowiedzialny za operowaniu głównie na obiektach typu Project oraz ProjectDto.
 *
 * @see pl.gatetologic.backend.springbootbackend.service.declaration.BaseService
 * @see pl.gatetologic.backend.springbootbackend.service.implementation.ProjectServiceImpl
 * @see pl.gatetologic.backend.springbootbackend.domain.Project
 * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
 */
public interface ProjectService extends BaseService<Project, ProjectDto, String> {
    /**
     *  Metoda zwracająca wszystkie projekty z bazy danych,
     *
     * @return zwraca listę wszystkich projektów zwróconych z bazy danych.
     */
    List<ProjectDto> findAll();
    /**
     *  Metoda aktualizująca reprezentacje układu w projekcie o identyfikatorze równym id.
     *
     * @param newRepresentation nowa reprezentacja do zastapienia w obiekcie Project
     * @param id identyfikator objektu do zaktualizowania
     * @return zwraca zaktualizowany ProjectDto
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.Project
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     */
    ProjectDto patchRepresentationByProjectId(String newRepresentation, String id);


    /**
     * Metoda zwracająca wszystkie projekty użytkownika o podanym loginie.
     *
     * @param login Login szukanego użytkownika
     * @return zwraca wszystkie projekty użytkownika o podanym loginie.
     *
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     */
    List<ProjectDto> findAllByUserLogin(String login);

    /**
     * Metoda do zapisywania ProjectDto do bazy przypisując jednocześnie przypisując dany Project do użytkownika.
     *
     * @param projectDto ProjectDto do zapisania
     * @param userDto użytkownik do którego zostanie przypisany zapisywany projekt.
     * @return zwraca zapisany projekt,
     *
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     * @see pl.gatetologic.backend.springbootbackend.dto.UserDto
     */
    ProjectDto saveDTOToUser(ProjectDto projectDto, UserDto userDto);

    /**
     * Metoda do zapisywania ProjectDto do bazy przypisując jednocześnie przypisując dany Project do użytkownika.
     *
     * @param projectDto ProjectDto do zapisania
     * @param login użytkownik do którego zostanie przypisany zapisywany projekt.
     * @return zwraca zapisany projekt,
     *
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     * @see pl.gatetologic.backend.springbootbackend.dto.UserDto
     */
    ProjectDto saveDTOToUser(ProjectDto projectDto, String login);
}
