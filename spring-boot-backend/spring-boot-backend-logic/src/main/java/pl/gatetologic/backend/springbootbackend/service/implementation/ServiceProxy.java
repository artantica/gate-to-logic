package pl.gatetologic.backend.springbootbackend.service.implementation;

import org.springframework.stereotype.Service;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService;
import pl.gatetologic.backend.springbootbackend.service.declaration.UserService;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Klasa odpowiedzialna za komunikację pomiędzy serwisami.
 * Używa adnotacji @Service dzięki czemu jest obsługiwany przez framework Spring Boot
 *
 * @see  pl.gatetologic.backend.springbootbackend.service.declaration.UserService
 * @see  pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService
 */
@Service
public class ServiceProxy {

    private UserService userService;
    private ProjectService projectService;

    void setUserService(UserService userService) {
        this.userService = userService;
    }
    void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Metoda zwracająca wszystkie projekty użytkownika wzgledem jego loginu.
     * Wykorzystuje wywołanie UserService.
     *
     * @param login Login uzytkownika, którego projekty chcemy zwrócić.
     * @return  zwraca listę projektów danego użytkownika.
     *
     *  @see  pl.gatetologic.backend.springbootbackend.service.declaration.UserService
     */
    List<ProjectDto> findAllProjectByUserLogin(String login){
        return userService.findByLogin(login).getProjects();
    }

    /**
     * Metoda odpowiedzialne za zapisanie projektu jednocześnie przypisując go do danego użytkownika.
     * Wykorzystuje wywoałania UserService oraz ProjectService.
     *
     * @param project ProjectDto do zapisania
     * @param user UserDto do którego ma zostać przypisany project.
     * @return zwraca zapisany ProjectDto.
     *
     * @see  pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     * @see  pl.gatetologic.backend.springbootbackend.dto.UserDto
     * @see  pl.gatetologic.backend.springbootbackend.service.declaration.UserService
     * @see  pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService
     */
    ProjectDto addProjectToUser(ProjectDto project, UserDto user){
        ProjectDto saved = projectService.saveDTO(project);
        user.getProjects().add(saved);
        userService.saveDTO(user);
        return saved;
    }

    /**
     * Metoda odpowiedzialne za zapisanie projektu jednocześnie przypisując go do danego użytkownika.
     * Wykorzystuje wywoałania UserService oraz ProjectService.
     *
     * @param project ProjectDto do zapisania
     * @param login Login użytkowanika, do którego ma zostać przypisany project.
     * @return zwraca zapisany ProjectDto.
     *
     * @see  pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     * @see  pl.gatetologic.backend.springbootbackend.dto.UserDto
     * @see  pl.gatetologic.backend.springbootbackend.service.declaration.UserService
     * @see  pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService
     */
    ProjectDto addProjectToUser(ProjectDto project, String login){
        ProjectDto saved = projectService.saveDTO(project);
        UserDto userDto = userService.findByLogin((login));
        userDto.getProjects().add(saved);
        userService.saveDTO(userDto);
        return saved;
    }

}
