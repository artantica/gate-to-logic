package pl.gatetologic.backend.springbootbackend.service.declaration;

/**
 * Bazowy serwis z rodzaju CRUD.
 *
 * @param <T> Bazowy typ serwisu.
 * @param <DTO> Typ DTO bazowego typu.
 * @param <ID> Typ id obiektu bazowego.
 *
 * @see org.springframework.data.repository.core.CrudMethods
 */
public interface BaseService<T, DTO, ID> {
    /**
     * Metoda znajdująca w bazie danych obiekt typu T z podanym identyfikatorem id.
     *
     * @param id Dany identyfikator względem którego szukany jest obiekt w bazie danych.
     * @return Zwraca znaleziony obiekt w postaci DTO.
     */
    DTO findById(ID id);

    /**
     *  Metoda zapisuje podany obiekt typu T w bazie oraz zwraca reprezentacje DTO zapisanego obiektu.
     *
     * @param object obiekt do zapisania.
     * @return Zwraca reprezentacje DTO zapisanego obiektu.
     */
    DTO save(T object);

    /**
     *  Metoda zapisuje podany obiekt typu DTO w bazie oraz zwraca reprezentacje DTO zapisanego obiektu.
     *
     * @param object obiekt do zapisania.
     * @return Zwraca reprezentacje DTO zapisanego obiektu.
     */
    DTO saveDTO(DTO object);

    /**
     * Metoda usuwa z bazy danych obiekt typu T o identyfikatorze danego id.
     *
     * @param id Identyfikator obiektu do usunięcia.
     */
    void deleteById(ID id);
}
