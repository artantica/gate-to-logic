package pl.gatetologic.backend.springbootbackend.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.domain.User;

/**
 * Interfejs mapująca obiekty klas User oraz UserDto.
 * Spring Boot wygeneruje implementację interfejsu jako Bean.
 * Klasa ta jest odpowiedzialna za mapowanie dwóch klas pomiędzy sobą.
 *
 * @see org.mapstruct.Mapper
 * @see pl.gatetologic.backend.springbootbackend.domain.User
 * @see pl.gatetologic.backend.springbootbackend.dto.UserDto
 * @see pl.gatetologic.backend.springbootbackend.mapper.ProjectMapper
 */
@Mapper(componentModel = "spring",injectionStrategy = InjectionStrategy.CONSTRUCTOR,  uses = {ProjectMapper.class})
public interface UserMapper {
    /**
     *  Mapuje wartości pól klasy User do pól klasy UserDto, tworząc nowy obiekt.
     *
     * @param user User do zmapowania na UserDto
     * @return zwraca zmapowaną instancję klasy UserDto.
     * Zwrócony obiekt nie posiada obiektów SystemDto wewnątrz elementów listy pola projects.
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.User
     * @see pl.gatetologic.backend.springbootbackend.dto.UserDto
     */
    UserDto toUserDto(User user);
    /**
     * Mapuje wartości pól klasy UserDto do pól klasy User, tworząc nowy obiekt.
     *
     *  @param userDto UserDto do zmapowania na User
     *  @return zwraca zmapowaną instancję klasy User
     *
     *  @see pl.gatetologic.backend.springbootbackend.domain.User
     *  @see pl.gatetologic.backend.springbootbackend.dto.UserDto
     */
    User toUser(UserDto userDto);
}
