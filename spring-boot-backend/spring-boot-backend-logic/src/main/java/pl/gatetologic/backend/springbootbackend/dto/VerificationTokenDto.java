package pl.gatetologic.backend.springbootbackend.dto;

import lombok.*;

import java.util.Date;

/**
 *  Klasa VerificationToken Data Transfer Object. Kopia obiektu VerificationToken.
 *  Instancje klasy DTO są używane do przesyłania danych i operowania na nich podczas modyfikowania,
 *  aby nie ruszać instancji bezpośrednio z bazy danych. Pola klasy DTO są to kopie pól klasy bazowej.
 *
 * @see lombok.Data
 * @see lombok.Builder
 * @see lombok.Getter
 * @see lombok.Setter
 * @see lombok.NoArgsConstructor
 * @see lombok.AllArgsConstructor
 * @see pl.gatetologic.backend.springbootbackend.domain.VerificationToken
 */
@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VerificationTokenDto {
    private String token;
    private Date expiryDate;
}
