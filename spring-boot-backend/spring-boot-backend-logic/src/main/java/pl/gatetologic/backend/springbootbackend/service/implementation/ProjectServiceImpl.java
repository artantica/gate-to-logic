package pl.gatetologic.backend.springbootbackend.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.gatetologic.backend.springbootbackend.domain.Project;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.exception.ObjectNotFoundException;
import pl.gatetologic.backend.springbootbackend.mapper.ProjectMapper;
import pl.gatetologic.backend.springbootbackend.repository.ProjectRepository;
import pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

/**
 *  Klasa serwisowa odpowiedzalna za operacje na obiektach typu Project oraz ProjectDTO.
 *  Implementuje interfejs ProjectService
 *  Używa on instancji ProjectMapper do mapowania pomiędzy dwoma typami klasy Project
 *  oraz używa ProjectRepository do komunikacji z bazą danych.
 *  Używa adnotacji @Service dzięki czemu jest obsługiwany przez framework Spring Boot
 *
 * @see pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService
 * @see pl.gatetologic.backend.springbootbackend.repository.ProjectRepository
 * @see pl.gatetologic.backend.springbootbackend.mapper.ProjectMapper
 * @see pl.gatetologic.backend.springbootbackend.domain.Project
 * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
 */
@Service
public class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;
    private final ServiceProxy proxy;
    private final ProjectMapper mapper;

    /**
     * Konstruktor używany przez SpringBoot.
     * Dzięki niemu jest użyte wstrzykiwanie zależności.
     *
     * @param projectRepository Bean repozytorium
     * @param proxy Bean komunikatora pomiędzy serwerami
     * @param mapper Bean mappera
     */
    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, ServiceProxy proxy, ProjectMapper mapper) {
        this.projectRepository = projectRepository;
        this.proxy = proxy;
        this.mapper = mapper;
    }

    /**
     * Metoda do ustawienia Beana ProjectService w ServiceProxy.
     * Niweluje zapętlenie zależności
     *
     * @see pl.gatetologic.backend.springbootbackend.service.implementation.ServiceProxy
     */
    @PostConstruct
    public void postConstruct(){
        this.proxy.setProjectService(this);
    }

    /**
     *  Metoda zwracająca wszystkie projekty z bazy danych,
     *  Metoda używa mappera do mapowania listy projektów uprzednio wywołując
     *  ProjectRepository do pobrania obiektów z bazy.
     *
     * @return zwraca listę wszystkich projektów zwróconych z bazy danych.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.ProjectMapper
     * @see pl.gatetologic.backend.springbootbackend.repository.ProjectRepository
     */
    @Override
    public List<ProjectDto> findAll() {    
        return mapper.toProjectDtoList(projectRepository.findAll());
    }


    /**
     * Metoda zwracająca wszystkie projekty użytkownika o podanym loginie.
     * Wywoływane jest ServiceProxy do komunikacji z UserService potrzebnym do znalezienia prawidłowego użytkownika.
     *
     * @param login Login szukanego użytkownika
     * @return zwraca wszystkie projekty użytkownika o podanym loginie.
     *
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     * @see pl.gatetologic.backend.springbootbackend.service.implementation.ServiceProxy
     */
    @Override
    public List<ProjectDto> findAllByUserLogin(String login) {
        return proxy.findAllProjectByUserLogin(login);
    }

    /**
     * Metoda znajdująca w bazie danych obiekt typu Project z podanym identyfikatorem id.
     * W przypadku nie znalezienia pożadanego obiektu zostanie wyrzucony RuntimeException typu ObjectNotFoundException.
     * Metoda wykorzystuje mapper oraz repositorium do pobrania obiektu z bazy a następnie zmapowaniu na obiekt DTO.
     *
     * @param id Dany identyfikator względem którego szukany jest obiekt w bazie danych.
     * @return Zwraca znaleziony obiekt w postaci DTO.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.ProjectMapper
     * @see pl.gatetologic.backend.springbootbackend.repository.ProjectRepository
     * @see pl.gatetologic.backend.springbootbackend.exception.ObjectNotFoundException
     */
    @Override
    public ProjectDto findById(String id) {
        Optional<Project> optionalProject = projectRepository.findById(id);
        return optionalProject.
                map(mapper::toProjectDto).
                orElseThrow(() -> new ObjectNotFoundException("Project with id="+id+" not found"));
    }

    /**
     *  Metoda zapisuje podany obiekt typu Project w bazie oraz zwraca reprezentacje DTO zapisanego obiektu.
     *  Używane jest odpowiednie repozytorium do zapisania w bazie, nastepnie odpowiedni mapper mapuje zapisany obiekt
     *  na postać DTO, któ©a jest ostatecznie zwracana.
     *
     * @param object obiekt do zapisania.
     * @return Zwraca reprezentacje DTO zapisanego obiektu.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.ProjectMapper
     * @see pl.gatetologic.backend.springbootbackend.repository.ProjectRepository
     */
    @Override
    public ProjectDto save(Project object) {
        return mapper.toProjectDto(projectRepository.save(object));
    }

    /**
     *  Metoda zapisuje podany obiekt typu ProjectDTO w bazie oraz zwraca reprezentacje DTO zapisanego obiektu.
     *  Używany jest mapper.
     *
     * @param object obiekt do zapisania.
     * @return Zwraca reprezentacje DTO zapisanego obiektu.
     *
     * @see pl.gatetologic.backend.springbootbackend.mapper.ProjectMapper
     */
    @Override
    public ProjectDto saveDTO(ProjectDto object) {
        return this.save(mapper.toProject(object));
    }

    /**
     * Metoda do zapisywania ProjectDto do bazy przypisując jednocześnie przypisując dany Project do użytkownika.
     * Wywoływany jest ServiceProxy do zapisania zaktualizowanego użytkownika po przypisaniu do niego nowego projektu.
     *
     * @param projectDto ProjectDto do zapisania
     * @param userDto użytkownik do którego zostanie przypisany zapisywany projekt.
     * @return zwraca zapisany projekt,
     *
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     * @see pl.gatetologic.backend.springbootbackend.dto.UserDto
     * @see pl.gatetologic.backend.springbootbackend.service.implementation.ServiceProxy
     */
    @Override
    public ProjectDto saveDTOToUser(ProjectDto projectDto, UserDto userDto) {
        return proxy.addProjectToUser(projectDto, userDto);
    }

    /**
     * Metoda do zapisywania ProjectDto do bazy przypisując jednocześnie przypisując dany Project do użytkownika.
     * Wywoływany jest ServiceProxy do zapisania zaktualizowanego użytkownika po przypisaniu do niego nowego projektu.
     *
     * @param projectDto ProjectDto do zapisania
     * @param login Login użytkownika, do którego zostanie przypisany zapisywany projekt.
     * @return zwraca zapisany projekt,
     *
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     * @see pl.gatetologic.backend.springbootbackend.dto.UserDto
     * @see pl.gatetologic.backend.springbootbackend.service.implementation.ServiceProxy
     */
    @Override
    public ProjectDto saveDTOToUser(ProjectDto projectDto, String login) {
        return proxy.addProjectToUser(projectDto, login);
    }

    /**
     * Metoda usuwa z bazy danych obiekt typu Project o identyfikatorze danego s.
     * Wywoływana jest metoda odpowiedniego repozytorium do usunięcia z bazy konkretnego zasobu.
     *
     * @param s Identyfikator obiektu do usunięcia.
     *
     * @see pl.gatetologic.backend.springbootbackend.repository.ProjectRepository
     */
    @Override
    public void deleteById(String s) {
        projectRepository.deleteById(s);
    }


    /**
     *  Metoda aktualizująca system w projekcie o identyfikatorze równym id.
     *  Wywoływany jest mapper.
     *
     * @param newRepresentation nowa reprezentacja do zastapienia w obiekcie Project
     * @param id identyfikator objektu do zaktualizowania
     * @return zwraca zaktualizowany ProjectDto
     *
     * @see pl.gatetologic.backend.springbootbackend.domain.Project
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     * @see pl.gatetologic.backend.springbootbackend.mapper.ProjectMapper
     */
    @Override
    public ProjectDto patchRepresentationByProjectId(String newRepresentation, String id) {
        ProjectDto toUpdate = this.findById(id);
        toUpdate.setRepresentation(newRepresentation);
        return this.save(mapper.toProject(toUpdate));
    }
}
