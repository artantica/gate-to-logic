package pl.gatetologic.backend.springbootbackend.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Klasa odpowiedzialna za generowanie, weryfikacje i pobieranie danych z dokenu JWT (JSON Web Token).
 * Korzysta z sekretnego klucza.
 * Adnotacja @Component wskazuje, że klasa będzie zarządzania przez kontekst Spring.
 * Adnotacja @Value wczytuje konfiguracje do pola klasy.
 *
 * @see org.springframework.stereotype.Component
 */
@Component
public class JwtTokenUtil implements Serializable {
    private static final long serialVersionUID = -2550185165626007488L;
    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    @Value("${jwt.secret}")
    private String secret;

    /**
     * Metoda służąca do wydobywania loginu użytkowanika z tokena.
     * @param token
     * @return
     */
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }


    /**
     * Metoda służąca do wydobywania daty ważności tokena z tokena.
     * @param token Token, z którego wydobywamy informacje o dacie ważności.
     * @return Date ważności tokena w postaci obiektu klasy Date.
     *
     * @see java.util.Date
     */
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    /**
     * Metoda pomocnicza pobierająca deklaracje z tokena
     * @param token Token, z któ®ego pobieramy deklaracje.
     * @param claimsResolver Funkcja określająca jakie informacje chemy wydobyć z tokena
     * @param <T> Zwracana klasa
     * @return Obiekt T
     */
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    /**
     * Metoda służąca do wydobywania informacji z tokena.
     * Używamy tu sekretnego klucza.
     * @param token Token, z którego wydobywamy informacje.
     * @return Informacje z tokena w postaci obiektu klasy Claims
     *
     * @see io.jsonwebtoken.Claims
     */
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    /**
     * Metoda służąca do sprawdzenia, czy token stracił ważność.
     * @param token Sprawdzany token
     * @return true jeśli token stracił ważnośc, w przeciwnym wypadku false.
     */
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     *  Metoda służąca do generowania tokena JWT dla danych użytkownika.
     * @param userDetails Dane uzytkownika
     * @return String Wygenerowany token.
     */
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, userDetails.getUsername());
    }

    /**
     * Metoda służąca do generowania tokena JWT.
     * Uzywany jest to algorytm HS512 i sekretny klucz
     * @param claims
     * @param subject
     * @return
     */
    private String doGenerateToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }


    /**
     * Metoda służąca do weryfikacji tokenu
     * @param token Token do zweryfikowania
     * @param userDetails Dane użytkownika
     * @return Boolean Zwraca true, jeśli token zostanie poprawnie zweryfikowany, w przeciwnym wypadku false.
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}
