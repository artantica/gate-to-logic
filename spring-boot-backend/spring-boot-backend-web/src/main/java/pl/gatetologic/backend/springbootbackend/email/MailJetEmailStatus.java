package pl.gatetologic.backend.springbootbackend.email;

import org.json.JSONArray;

/**
 * Klasa określająca status wysłania maila - powodzenie lub niepowodzenie.
 */
public class MailJetEmailStatus {
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";

    private final String to;
    private final String subject;
    private final String body;

    private String status;
    private String message;

    public MailJetEmailStatus(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    /**
     * Metoda zwracająca informacje o powodzeniu wysłania maila.
     * @param message
     * @return
     */
    public MailJetEmailStatus success(JSONArray message) {
        this.message = message.toString();
        this.status = SUCCESS;
        return this;
    }

    /**
     * Metoda zwracająca informacje o niepowodzeniu wysłania maila.
     * @param errorMessage
     * @return
     */
    public MailJetEmailStatus error(String errorMessage) {
        this.status = ERROR;
        this.message = errorMessage;
        return this;
    }

    /**
     * Metoda zamieniająca status na tekst.
     * @return String
     */
    @Override
    public String toString() {
        return "MailJetEmailStatus{" +
                "to='" + to + '\'' +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
