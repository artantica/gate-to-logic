package pl.gatetologic.backend.springbootbackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;
import pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa odpowiadająca za obsługę zapytań dotyczących projektów.
 * Adnotacja @RestController oznacza kontroler, tj. klasa, która będzie obsługiwała zapytania
 * wysyłane poprzez przeglądarkę od użytkowników.
 * Adnotacja @CrossOrigin() pozwala na wykonanie żądania HTTP na zastrzeżonych zasobach z serwera o innej domenie
 * niż domena serwera, który udostępnia te zasoby, dzięku czemu obsłużone zostanie żądanie z naszego frontendu w Angularze.
 *
 * @see org.springframework.web.bind.annotation.RestController
 * @see org.springframework.web.bind.annotation.CrossOrigin
 */
@CrossOrigin(methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PATCH})
@RestController
@RequestMapping("project")
public class ProjectController {
    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Metodą obsługująca zapytanie GET /all.
     * Zwraca wszsytkie projekty użutkownika.
     * @param authentication
     * @return
     */
    @GetMapping("/all")
    public ResponseEntity<List<ProjectDto>> findAllByUserLogin(Authentication authentication){
        System.out.println(authentication.getName());
        return new ResponseEntity<>(projectService.findAllByUserLogin(authentication.getName()), HttpStatus.OK);
    }

    /**
     * Metodą obsługująca zapytanie GET /get/{project_id}.
     * Zwraca projekt o podanym identyfikatorze jako obiekt klasy ProjectDto.
     * @param project_id Identyfikator projektu.
     * @return Odpowiedź zwracająca projekt o podanym id jako obiekt klasy ProjectDto.
     *
     * @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     */
    @GetMapping("/get/{project_id}")
    public ResponseEntity<ProjectDto> getProjectById(@PathVariable String project_id){
        return new ResponseEntity<>(projectService.findById(project_id), HttpStatus.OK);
    }

    /**
     * Metodą obsługująca zapytanie GET /new.
     * Odpowiada za dodanie nowego projektu do bazy.
     * @param projectDto Nowy projekt
     * @param authentication
     * @return Odpowiedź zwracająca nowy projekt jako obiekt klasy ProjectDto.
     *
     *  @see pl.gatetologic.backend.springbootbackend.dto.ProjectDto
     */
    @PostMapping("/new")
    public ResponseEntity<ProjectDto> addNewProject(@RequestBody ProjectDto projectDto, Authentication authentication) { ;
      return new ResponseEntity<>(projectService.saveDTOToUser(projectDto, authentication.getName()), HttpStatus.CREATED);
    }

    /**
     * Metodą obsługująca zapytanie PATCH /save/{project_id}.
     * Odpowiada za auktualnienie reprezentacji układu projektu o podanym indentifikatorze.
     * @param newRepresentation Nowa reprexentacja układu.
     * @param project_id Identyfikator projektu.
     * @return Odpowiedź zwracająca zaktualizowany projekt jako obiekt klasy ProjectDto.
     */
    @PatchMapping("/save/{project_id}")
    public ResponseEntity<ProjectDto> patchSystemByProjectId(@RequestBody String newRepresentation,
                                                             @PathVariable String project_id){
        return new ResponseEntity<>(projectService.patchRepresentationByProjectId(newRepresentation, project_id), HttpStatus.OK);
    }

}
