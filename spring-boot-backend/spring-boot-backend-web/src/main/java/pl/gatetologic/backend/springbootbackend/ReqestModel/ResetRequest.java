package pl.gatetologic.backend.springbootbackend.ReqestModel;

import java.io.Serializable;

public class ResetRequest implements Serializable {
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String token;
    private String password;
}
