package pl.gatetologic.backend.springbootbackend.email;

import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Email;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Klasa odpowiadająca za wysyłanie maili z linkiem aktywacyjnych oraz resetujących hasło.
 */
@Component
public class MailJetEmail {
    private MailJetConfig config;

    @Value("${mailJet.website}")
    private String website;

    @Value("${mailJet.fromEmail}")
    private String helpMail;


    @Autowired
    public MailJetEmail(MailJetConfig config) {
        this.config = config;
    }

    /**
     * Metoda służąda to wysłania maila z linkiem aktywacyjnym.
     * @param emailTo Adres odbiorcy maila.
     * @param login Login użytkownika
     * @param link link aktywacyjny
     * @return MailJetEmailStatus Status wysłania maila.
     */
    public MailJetEmailStatus sendActivationEmail(String emailTo, String login, String link) {
        try {

            MailjetClient client = new MailjetClient(
                    config.getMailJetApiKey(),
                    config.getMailJetApiSecret()
            );
            String templateConfirm = "\n" +
                                             "<!DOCTYPE html>\n" +
                                             "<html>\n" +
                                             "  <head>\n" +
                                             "    <title>Please confirm your e-mail</title>\n" +
                                             "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                                             "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                                             "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                                             "    <style type=\"text/css\">\n" +
                                             "      body,table,td,a{\n" +
                                             "      -webkit-text-size-adjust:100%;\n" +
                                             "      -ms-text-size-adjust:100%;\n" +
                                             "      }\n" +
                                             "      table,td{\n" +
                                             "      mso-table-lspace:0pt;\n" +
                                             "      mso-table-rspace:0pt;\n" +
                                             "      }\n" +
                                             "      img{\n" +
                                             "      -ms-interpolation-mode:bicubic;\n" +
                                             "      }\n" +
                                             "      img{\n" +
                                             "      border:0;\n" +
                                             "      height:auto;\n" +
                                             "      line-height:100%;\n" +
                                             "      outline:none;\n" +
                                             "      text-decoration:none;\n" +
                                             "      }\n" +
                                             "      table{\n" +
                                             "      border-collapse:collapse !important;\n" +
                                             "      }\n" +
                                             "      body{\n" +
                                             "      height:100% !important;\n" +
                                             "      margin:0 !important;\n" +
                                             "      padding:0 !important;\n" +
                                             "      width:100% !important;\n" +
                                             "      }\n" +
                                             "      a[x-apple-data-detectors]{\n" +
                                             "      color:inherit !important;\n" +
                                             "      text-decoration:none !important;\n" +
                                             "      font-size:inherit !important;\n" +
                                             "      font-family:inherit !important;\n" +
                                             "      font-weight:inherit !important;\n" +
                                             "      line-height:inherit !important;\n" +
                                             "      }\n" +
                                             "      a{\n" +
                                             "      color:#00bc87;\n" +
                                             "      text-decoration:underline;\n" +
                                             "      }\n" +
                                             "      * img[tabindex=0]+div{\n" +
                                             "      display:none !important;\n" +
                                             "      }\n" +
                                             "      @media screen and (max-width:350px){\n" +
                                             "      h1{\n" +
                                             "      font-size:24px !important;\n" +
                                             "      line-height:24px !important;\n" +
                                             "      }\n" +
                                             "      }   div[style*=margin: 16px 0;]{\n" +
                                             "      margin:0 !important;\n" +
                                             "      }\n" +
                                             "      @media screen and (min-width: 360px){\n" +
                                             "      .headingMobile {\n" +
                                             "      font-size: 40px !important;\n" +
                                             "      }\n" +
                                             "      .headingMobileSmall {\n" +
                                             "      font-size: 28px !important;\n" +
                                             "      }\n" +
                                             "      }\n" +
                                             "    </style>\n" +
                                             "  </head>\n" +
                                             "  <body bgcolor=\"#ffffff\" style=\"background-color: #ffffff; margin: 0 !important; padding: 0 !important;\">\n" +
                                             "    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\"> - aby zakończyć rejestracje, musisz potwierdzić, że otrzymałeś ten mail w ciągu 24 godzin. Aby potwierdzić prosze naciśnij przycisk POTWIERDŹ MAIL.</div>\n" +
                                             "    <center>\n" +
                                             "      <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" valign=\"top\">\n" +
                                             "        <tbody>\n" +
                                             "          <tr>\n" +
                                             "            <td>\n" +
                                             "              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" valign=\"top\" bgcolor=\"#ffffff\" style=\"padding: 0 20px !important;max-width: 500px;width: 90%;\">\n" +
                                             "                <tbody>\n" +
                                             "                  <tr>\n" +
                                             "                    <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 10px 0 0px 0;\"><!--[if (gte mso 9)|(IE)]><table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"350\">\n" +
                                             "<tr>\n" +
                                             "<td align=\"center\" valign=\"top\" width=\"350\">\n" +
                                             "<![endif]-->\n" +
                                             "                      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;border-bottom: 1px solid #e4e4e4 ;\">\n" +
                                             "                        <tbody>\n" +
                                             "                          <tr>\n" +
                                             "                            <td bgcolor=\"#ffffff\" align=\"left\" valign=\"middle\" style=\"padding: 0px; color: #111111; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; line-height: 62px;padding:0 0 15px 0;\"><a href=\""+ website + "\" target=\"_blank\"><img width=\"19\" height=\"25\" alt=\"logo\" src=\"https://s3-eu-west-1.amazonaws.com/avocode-mailing/mailing-app/img/logo.png\"></a></td>\n" +
                                             "                            <td bgcolor=\"#ffffff\" align=\"right\" valign=\"middle\" style=\"padding: 0px; color: #111111; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; line-height: 48px;padding:0 0 15px 0;\"><a href=\"" + website +"/login/\" target=\"_blank\" style=\"font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #797979;font-size: 12px;font-weight:400;-webkit-font-smoothing:antialiased;text-decoration: none;\">Login to gateToLogic</a></td>\n" +
                                             "                          </tr>\n" +
                                             "                        </tbody>\n" +
                                             "                      </table>\n" +
                                             "                    </td>\n" +
                                             "                  </tr>\n" +
                                             "                  <tr>\n" +
                                             "                    <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 0;\"><!--[if (gte mso 9)|(IE)]><table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"350\">\n" +
                                             "<tr>\n" +
                                             "<td align=\"center\" valign=\"top\" width=\"350\">\n" +
                                             "<![endif]-->\n" +
                                             "                      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;border-bottom: 1px solid #e4e4e4;\">\n" +
                                             "                        <tbody>\n" +
                                             "                          <tr>\n" +
                                             "                            <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 0 0 0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400;-webkit-font-smoothing:antialiased;\">\n" +
                                             "                                                <p class=\"headingMobile\" style=\"margin: 0;color: #171717;font-size: 26px;font-weight: 200;line-height: 130%;margin-bottom:5px;\">Potwierdź swój email żeby ukończyć rejestrację na GateToLogic</p>\n" +
                                             "                            </td>\n" +
                                             "                          </tr>\n" +
                                             "                                            <tr>\n" +
                                             "                                              <td height=\"20\"></td>\n" +
                                             "                                            </tr>\n" +
                                             "                          <tr>\n" +
                                             "                            <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding:0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400;-webkit-font-smoothing:antialiased;\">\n" +
                                             "                                                <p style=\"margin:0;color:#585858;font-size:14px;font-weight:400;line-height:170%;\">Witaj <b>"+ login+"!</b></p>\n" +
                                             "                                                <p style=\"margin:0;color:#585858;font-size:14px;font-weight:400;line-height:170%;\">Dziękujemy za wybranie GateToLogic.</p>\n" +
                                             "                                                <p style=\"margin:0;margin-top:20px;line-height:0;\"></p>\n" +
                                             "                                                <p style=\"margin:0;color:#585858;font-size:14px;font-weight:400;line-height:170%;\">Proszę potwierdź, że <b>"+ emailTo +"</b> jest twoim adresem mailowym poprzez kliknięcie przycisku poniżej lub użyj tego linku <a style='color: #00bc87;text-decoration: underline;' target='_blank' href='"+ link +"'>" + link +"</a> <br /> w ciągu 24&nbsp;godzin.</p>\n" +
                                             "                            </td>\n" +
                                             "                          </tr>\n" +
                                             "                                            <tr>\n" +
                                             "                                              <td align=\"center\">\n" +
                                             "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                                             "                                                  <tr>\n" +
                                             "                                                    <td align=\"center\" style=\"padding: 33px 0 33px 0;\">\n" +
                                             "                                                      <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n" +
                                             "                                                        <tr>\n" +
                                             "                                                          <td align=\"center\" style=\"border-radius: 4px;\" bgcolor=\"#00bc87\"><a href=\"https://avocode.com/\" style=\"text-transform:uppercase;background:#00bc87;font-size: 13px; font-weight: 700; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none !important; padding: 20px 25px; border-radius: 4px; border: 1px solid #00bc87; display: block;-webkit-font-smoothing:antialiased;\" target=\"_blank\"><span style=\"color: #ffffff;text-decoration: none;\">POTWIERDŹ MAIL</span></a></td>\n" +
                                             "                                                        </tr>\n" +
                                             "                                                      </table>\n" +
                                             "                                                    </td>\n" +
                                             "                                                  </tr>\n" +
                                             "                                                </table>\n" +
                                             "                                              </td>\n" +
                                             "                                            </tr>\n" +
                                             "                        </tbody>\n" +
                                             "                      </table>\n" +
                                             "                    </td>\n" +
                                             "                  </tr>\n" +
                                             "                  <tr>\n" +
                                             "                    <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 0;\"><!--[if (gte mso 9)|(IE)]><table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"350\">\n" +
                                             "<tr>\n" +
                                             "<td align=\"center\" valign=\"top\" width=\"350\">\n" +
                                             "<![endif]-->\n" +
                                             "                      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\">\n" +
                                             "                        <tbody>\n" +
                                             "                          <tr>\n" +
                                             "                            <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 30px 0 30px 0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 18px;\">\n" +
                                             "                                                <p style=\"margin: 0;color: #585858;font-size: 12px;font-weight: 400;-webkit-font-smoothing:antialiased;line-height: 170%;\">Potrzebujesz pomocy? Napisz do nas <a href=mailto:\""+ helpMail+"\"</a> albo odwiedź nasze <a href=\""+website+"\" style=\"color: #00bc87;text-decoration: underline;\" target=\"_blank\">Centrum pomocy</a></p>\n" +
                                             "                                                <tr>\n" +
                                             "                                                  <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 18px;\">\n" +
                                             "                                                    <p style=\"margin: 0;color: #585858;font-size: 12px;font-weight: 400;-webkit-font-smoothing:antialiased;line-height: 170%;\"></p>\n" +
                                             "                                                  </td>\n" +
                                             "                                                </tr>\n" +
                                             "                                                <tr>\n" +
                                             "                                                  <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px 0 30px 0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 18px;\">\n" +
                                             "                                                    <p style=\"margin: 0;color: #585858;font-size: 12px;font-weight: 400;-webkit-font-smoothing:antialiased;line-height: 170%;\">GTL, Inc.<br> 330 East 59th Street, 7th Floor<br> New York, NY 10022, USA</p>\n" +
                                             "                                                  </td>\n" +
                                             "                                                </tr>\n" +
                                             "                            </td>\n" +
                                             "                          </tr>\n" +
                                             "                        </tbody>\n" +
                                             "                      </table><!--[if (gte mso 9)|(IE)]></td></tr></table>\n" +
                                             "<![endif]-->\n" +
                                             "                    </td>\n" +
                                             "                  </tr>\n" +
                                             "                </tbody>\n" +
                                             "              </table>\n" +
                                             "            </td>\n" +
                                             "          </tr>\n" +
                                             "        </tbody>\n" +
                                             "      </table>\n" +
                                             "    </center>\n" +
                                             "\n" +
                                             "\040\040\n" +
                                             "  <script data-cfasync=\"false\" src=\"/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js\"></script></body>\n" +
                                             "</html>\n" +
                                             "\n";

            String subject = "Aktywuj swoje konto w GTL";
            MailjetRequest request = new MailjetRequest(Email.resource)
                    .property(Email.FROMEMAIL, config.getMailJetFromEmail())
                    .property(Email.FROMNAME, config.getMailJetFromName())
                    .property(Email.SUBJECT, subject)
//                    .property(Email.TEXTPART, body)
                    .property(Email.HTMLPART, templateConfirm)
//                    .property(Email.HTMLPART, body)
                    .property(
                            Email.RECIPIENTS,
                            new JSONArray().put(new JSONObject().put("Email", emailTo))
                    );

            MailjetResponse response = client.post(request);

            return new MailJetEmailStatus(emailTo, subject, "").success(response.getData());

        } catch (MailjetSocketTimeoutException | MailjetException e) {
            return new MailJetEmailStatus(emailTo, "Aktywuj swoje konto w GTL", "").error(e.getMessage());
        }
    }

    /**
     * Metoda służąda to wysłania maila z resetującym hasło.
     * @param emailTo Adres odbiorcy maila.
     * @param login Login użytkownika
     * @param link link zmieniejący hasło
     * @return MailJetEmailStatus Status wysłania maila.
     */
    public MailJetEmailStatus sendResetPasswordLink(String emailTo, String login, String link) {
        try {

            MailjetClient client = new MailjetClient(
                    config.getMailJetApiKey(),
                    config.getMailJetApiSecret()
            );

            String templateReset = "\n<!DOCTYPE html>\n" +
                                   "<html>\n" +
                                   "  <head>\n" +
                                   "    <title>Please confirm your e-mail</title>\n" +
                                   "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                                   "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                                   "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                                   "    <style type=\"text/css\">\n" +
                                   "      body,table,td,a{\n" +
                                   "      -webkit-text-size-adjust:100%;\n" +
                                   "      -ms-text-size-adjust:100%;\n" +
                                   "      }\n" +
                                   "      table,td{\n" +
                                   "      mso-table-lspace:0pt;\n" +
                                   "      mso-table-rspace:0pt;\n" +
                                   "      }\n" +
                                   "      img{\n" +
                                   "      -ms-interpolation-mode:bicubic;\n" +
                                   "      }\n" +
                                   "      img{\n" +
                                   "      border:0;\n" +
                                   "      height:auto;\n" +
                                   "      line-height:100%;\n" +
                                   "      outline:none;\n" +
                                   "      text-decoration:none;\n" +
                                   "      }\n" +
                                   "      table{\n" +
                                   "      border-collapse:collapse !important;\n" +
                                   "      }\n" +
                                   "      body{\n" +
                                   "      height:100% !important;\n" +
                                   "      margin:0 !important;\n" +
                                   "      padding:0 !important;\n" +
                                   "      width:100% !important;\n" +
                                   "      }\n" +
                                   "      a[x-apple-data-detectors]{\n" +
                                   "      color:inherit !important;\n" +
                                   "      text-decoration:none !important;\n" +
                                   "      font-size:inherit !important;\n" +
                                   "      font-family:inherit !important;\n" +
                                   "      font-weight:inherit !important;\n" +
                                   "      line-height:inherit !important;\n" +
                                   "      }\n" +
                                   "      a{\n" +
                                   "      color:#00bc87;\n" +
                                   "      text-decoration:underline;\n" +
                                   "      }\n" +
                                   "      * img[tabindex=0]+div{\n" +
                                   "      display:none !important;\n" +
                                   "      }\n" +
                                   "      @media screen and (max-width:350px){\n" +
                                   "      h1{\n" +
                                   "      font-size:24px !important;\n" +
                                   "      line-height:24px !important;\n" +
                                   "      }\n" +
                                   "      }   div[style*=margin: 16px 0;]{\n" +
                                   "      margin:0 !important;\n" +
                                   "      }\n" +
                                   "      @media screen and (min-width: 360px){\n" +
                                   "      .headingMobile {\n" +
                                   "      font-size: 40px !important;\n" +
                                   "      }\n" +
                                   "      .headingMobileSmall {\n" +
                                   "      font-size: 28px !important;\n" +
                                   "      }\n" +
                                   "      }\n" +
                                   "    </style>\n" +
                                   "  </head>\n" +
                                   "  <body bgcolor=\"#ffffff\" style=\"background-color: #ffffff; margin: 0 !important; padding: 0 !important;\">\n" +
                                   "    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\"> - aby zakończyć rejestracje, musisz potwierdzić, że otrzymałeś ten mail w ciągu 24 godzin. Aby potwierdzić prosze naciśnij przycisk POTWIERDŹ MAIL.</div>\n" +
                                   "    <center>\n" +
                                   "      <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" valign=\"top\">\n" +
                                   "        <tbody>\n" +
                                   "          <tr>\n" +
                                   "            <td>\n" +
                                   "              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" valign=\"top\" bgcolor=\"#ffffff\" style=\"padding: 0 20px !important;max-width: 500px;width: 90%;\">\n" +
                                   "                <tbody>\n" +
                                   "                  <tr>\n" +
                                   "                    <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 10px 0 0px 0;\"><!--[if (gte mso 9)|(IE)]><table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"350\">\n" +
                                   "<tr>\n" +
                                   "<td align=\"center\" valign=\"top\" width=\"350\">\n" +
                                   "<![endif]-->\n" +
                                   "                      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;border-bottom: 1px solid #e4e4e4 ;\">\n" +
                                   "                        <tbody>\n" +
                                   "                          <tr>\n" +
                                   "                            <td bgcolor=\"#ffffff\" align=\"left\" valign=\"middle\" style=\"padding: 0px; color: #111111; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; line-height: 62px;padding:0 0 15px 0;\"><a href=\"" + website + "\" target=\"_blank\"><img width=\"19\" height=\"25\" alt=\"logo\" src=\"https://s3-eu-west-1.amazonaws.com/avocode-mailing/mailing-app/img/logo.png\"></a></td>\n" +
                                   "                            <td bgcolor=\"#ffffff\" align=\"right\" valign=\"middle\" style=\"padding: 0px; color: #111111; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; line-height: 48px;padding:0 0 15px 0;\"><a href=\"" + website + "/login/\" target=\"_blank\" style=\"font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color: #797979;font-size: 12px;font-weight:400;-webkit-font-smoothing:antialiased;text-decoration: none;\">Reset password to gateToLogic</a></td>\n" +
                                   "                          </tr>\n" +
                                   "                        </tbody>\n" +
                                   "                      </table>\n" +
                                   "                    </td>\n" +
                                   "                  </tr>\n" +
                                   "                  <tr>\n" +
                                   "                    <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 0;\"><!--[if (gte mso 9)|(IE)]><table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"350\">\n" +
                                   "<tr>\n" +
                                   "<td align=\"center\" valign=\"top\" width=\"350\">\n" +
                                   "<![endif]-->\n" +
                                   "                      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;border-bottom: 1px solid #e4e4e4;\">\n" +
                                   "                        <tbody>\n" +
                                   "                          <tr>\n" +
                                   "                            <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding: 20px 0 0 0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400;-webkit-font-smoothing:antialiased;\">\n" +
                                   "                                                <p class=\"headingMobile\" style=\"margin: 0;color: #171717;font-size: 26px;font-weight: 200;line-height: 130%;margin-bottom:5px;\">Witaj " + login + "!</p>\n" +
                                   "                            </td>\n" +
                                   "                          </tr>\n" +
                                   "                                            <tr>\n" +
                                   "                                              <td height=\"20\"></td>\n" +
                                   "                                            </tr>\n" +
                                   "                          <tr>\n" +
                                   "                            <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding:0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400;-webkit-font-smoothing:antialiased;\">\n" +
                                   "                                                <p style=\"margin:0;color:#585858;font-size:14px;font-weight:400;line-height:170%;\">Zgłoszono prośbę o zmiene hasła w serwisie GateToLogic, w której posiadasz konto.</p>\n" +
                                   "                                                <p style=\"margin:0;margin-top:20px;line-height:0;\"></p>\n" +
                                   "                                                <p style=\"margin:0;color:#585858;font-size:14px;font-weight:400;line-height:170%;\">Aby kontynuować procedurę resetowania hasła kliknij przycisk poniżej lub użyj tego linku <a style='color: #00bc87;text-decoration: underline;' target='_blank' href='" + link + "'>" + link + "</a> <br />w ciągu 24&nbsp;godzin. Po tym czasie procedurę należy powtórzyć od początku</p>\n" +
                                   "                            </td>\n" +
                                   "                          </tr>\n" +
                                   "                                            <tr>\n" +
                                   "                                              <td align=\"center\">\n" +
                                   "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                                   "                                                  <tr>\n" +
                                   "                                                    <td align=\"center\" style=\"padding: 33px 0 33px 0;\">\n" +
                                   "                                                      <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n" +
                                   "                                                        <tr>\n" +
                                   "                                                          <td align=\"center\" style=\"border-radius: 4px;\" bgcolor=\"#00bc87\"><a href=\"https://avocode.com/\" style=\"text-transform:uppercase;background:#00bc87;font-size: 13px; font-weight: 700; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none !important; padding: 20px 25px; border-radius: 4px; border: 1px solid #00bc87; display: block;-webkit-font-smoothing:antialiased;\" target=\"_blank\"><span style=\"color: #ffffff;text-decoration: none;\">ZMIEŃ HASŁO</span></a></td>\n" +
                                   "                                                        </tr>\n" +
                                   "                           <tr>\n" +
                                   "                            <td bgcolor=\"#ffffff\" align=\"left\" style=\"padding:0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400;-webkit-font-smoothing:antialiased;\">\n" +
                                   "                                                <p style=\"margin:0;color:#585858;font-size:14px;font-weight:400;line-height:170%;\"><br/>Jeśli rezygnujesz z resetowania Twojego hasła, zignoruj tego maila.</p>\n" +
                                   "                                                <p style=\"margin:0;margin-top:20px;line-height:0;\"></p>\n" +
                                   "                                                <p style=\"margin:0;color:#585858;font-size:14px;font-weight:400;line-height:170%;\">Jeśli podejrzewasz, że ktoś próbował zresetować Twoje hasło, skontaktuj się z administratorem GTL.</p>\n" +
                                   "                            </td>\n" +
                                   "                          </tr>\n" +
                                   "                                                      </table>\n" +
                                   "                                                    </td>\n" +
                                   "                                                  </tr>\n" +
                                   "                                                </table>\n" +
                                   "                                              </td>\n" +
                                   "                                            </tr>\n" +
                                   "                        </tbody>\n" +
                                   "                      </table>\n" +
                                   "                    </td>\n" +
                                   "                  </tr>\n" +
                                   "                  <tr>\n" +
                                   "                    <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 0;\"><!--[if (gte mso 9)|(IE)]><table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"350\">\n" +
                                   "<tr>\n" +
                                   "<td align=\"center\" valign=\"top\" width=\"350\">\n" +
                                   "<![endif]-->\n" +
                                   "                      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\">\n" +
                                   "                        <tbody>\n" +
                                   "                          <tr>\n" +
                                   "                            <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 30px 0 30px 0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 18px;\">\n" +
                                   "                                                <p style=\"margin: 0;color: #585858;font-size: 12px;font-weight: 400;-webkit-font-smoothing:antialiased;line-height: 170%;\">Potrzebujesz pomocy? Napisz do nas <a href=mailto:\""+ helpMail+"\"</a> albo odwiedź nasz <a href=\""+website+"\" style=\"color: #00bc87;text-decoration: underline;\" target=\"_blank\">Centrum pomocy</a></p>\n" +
                                   "                                                <tr>\n" +
                                   "                                                  <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 18px;\">\n" +
                                   "                                                    <p style=\"margin: 0;color: #585858;font-size: 12px;font-weight: 400;-webkit-font-smoothing:antialiased;line-height: 170%;\"></p>\n" +
                                   "                                                  </td>\n" +
                                   "                                                </tr>\n" +
                                   "                                                <tr>\n" +
                                   "                                                  <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 15px 0 30px 0; color: #666666; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 18px;\">\n" +
                                   "                                                    <p style=\"margin: 0;color: #585858;font-size: 12px;font-weight: 400;-webkit-font-smoothing:antialiased;line-height: 170%;\">GTL, Inc.<br> 330 East 59th Street, 7th Floor<br> New York, NY 10022, USA</p>\n" +
                                   "                                                  </td>\n" +
                                   "                                                </tr>\n" +
                                   "                            </td>\n" +
                                   "                          </tr>\n" +
                                   "                        </tbody>\n" +
                                   "                      </table><!--[if (gte mso 9)|(IE)]></td></tr></table>\n" +
                                   "<![endif]-->\n" +
                                   "                    </td>\n" +
                                   "                  </tr>\n" +
                                   "                </tbody>\n" +
                                   "              </table>\n" +
                                   "            </td>\n" +
                                   "          </tr>\n" +
                                   "        </tbody>\n" +
                                   "      </table>\n" +
                                   "    </center>\n" +
                                   "\n" +
                                   "\040\040\n" +
                                   "  <script data-cfasync=\"false\" src=\"/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js\"></script></body>\n" +
                                   "</html>\n" +
                                   "\n";

            String subject = "Prośba o zmiane hasła";
            MailjetRequest request = new MailjetRequest(Email.resource)
                    .property(Email.FROMEMAIL, config.getMailJetFromEmail())
                    .property(Email.FROMNAME, config.getMailJetFromName())
                    .property(Email.SUBJECT, subject)
                    .property(Email.HTMLPART, templateReset)
                    .property(
                            Email.RECIPIENTS,
                            new JSONArray().put(new JSONObject().put("Email", emailTo))
                    );

            MailjetResponse response = client.post(request);

            return new MailJetEmailStatus(emailTo, subject, "").success(response.getData());

        } catch (MailjetSocketTimeoutException | MailjetException e) {
            return new MailJetEmailStatus(emailTo, "Aktywuj swoje konto w GTL", "").error(e.getMessage());
        }
    }

}
