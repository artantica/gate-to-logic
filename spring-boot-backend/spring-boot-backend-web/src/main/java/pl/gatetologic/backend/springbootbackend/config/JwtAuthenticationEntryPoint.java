package pl.gatetologic.backend.springbootbackend.config;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import pl.gatetologic.backend.springbootbackend.model.ApiResponse;

/**
 * Klasa implementująca interfejsy AuthenticationEntryPoint i Serializable.
 * Klasa odpowiada wstępną autoryzacje zapytań i wysłanie błędu o braku autoryzacji, jesli taki zajdzie.
 * Adnotacja @Component wskazuje, że klasa będzie zarządzania przez kontekst Spring.
 *
 * @see org.springframework.security.web.AuthenticationEntryPoint
 * @see org.springframework.stereotype.Component
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = -7858869558953243875L;

    /**
     * Metoda, która nadpisujemy w celu wygenerowania błędu o nieautoryzacji.
     * @param request
     * @param response
     * @param authException
     * @throws IOException
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        String messageError = authException.getMessage();
        if (messageError.contains("Bad credentials")) {
            messageError = "Zły login lub hasło. Spróbuj ponownie!";
        }

        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, messageError);
    }
}