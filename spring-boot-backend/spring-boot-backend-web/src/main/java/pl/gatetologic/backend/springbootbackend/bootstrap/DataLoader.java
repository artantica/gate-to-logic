package pl.gatetologic.backend.springbootbackend.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.gatetologic.backend.springbootbackend.domain.Project;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;
import pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService;
import pl.gatetologic.backend.springbootbackend.service.declaration.UserService;

import java.util.List;

/**
 *  DataLoader
 *
 *  Klasa implementująca {@link CommandLineRunner}.
 *  Celem jej jest załadowanie wstępnych danych do bazy w trakcie uruchamiania się aplikacji.
 *
 *  Adnotacja @Component zapewnia utworzenia się instancji jako Bean w trakcie uruchamiania się.
 * @see Component
 */
@Component
public class DataLoader implements CommandLineRunner {


    public static final String ID = "1";
    public static final String SOME_REPRESENTATION =
            "{\"elements\":[{\"id\":0,\"type\":12,\"x\":10,\"y\":10,\"in\":[],\"out\":[0]},{\"id\":1,\"type\":12,\"x\":10,\"y\":25,\"in\":[],\"out\":[1]},{\"id\":2,\"type\":2,\"x\":25,\"y\":15,\"in\":[0,1],\"out\":[2]},{\"id\":3,\"type\":3,\"x\":40,\"y\":15,\"in\":[2],\"out\":[3]},{\"id\":4,\"type\":12,\"x\":10,\"y\":40,\"in\":[],\"out\":[4]},{\"id\":5,\"type\":1,\"x\":55,\"y\":25,\"in\":[3,4],\"out\":[5]},{\"id\":6,\"type\":3,\"x\":70,\"y\":25,\"in\":[5],\"out\":[null]}],\"wires\":[{\"id\":0,\"val\":false,\"in\":0,\"out\":2},{\"id\":1,\"val\":false,\"in\":1,\"out\":2},{\"id\":2,\"val\":false,\"in\":2,\"out\":3},{\"id\":3,\"val\":false,\"in\":3,\"out\":5},{\"id\":4,\"val\":false,\"in\":4,\"out\":5},{\"id\":5,\"val\":false,\"in\":5,\"out\":6}]}";
    public static final String SOME_REPRESENTATION2 =
            "{\"elements\":[{\"id\":0,\"type\":12,\"x\":10,\"y\":10,\"in\":[],\"out\":[]},{\"id\":1,\"type\":12,\"x\":10,\"y\":25,\"in\":[],\"out\":[]},{\"id\":2,\"type\":2,\"x\":25,\"y\":15,\"in\":[],\"out\":[]},{\"id\":3,\"type\":3,\"x\":40,\"y\":15,\"in\":[],\"out\":[]}],\"wires\":[]}";
    public static final String ID2 = "2";
    public static final String NAME = "name1";
    public static final String SOME_DESCRIPTION = "some description";
    public static final String REPRESENTATION = "representatiom";
    public static final String LOGIN = "login3";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email@email.com";

    private final UserService userService;
    private final ProjectService projectService;

    /**
     * Konstruktor, który jest używany podczas wstrzykiwania zależności w trakcie tworzenia się Beana.
     *
     * @param userService UserService ustawiany za pomocą Dependency Injection.
     * @param projectService ProjectService ustawiany za pomocą Dependency Injection.
     */
    public DataLoader(UserService userService, ProjectService projectService) {
        this.userService = userService;
        this.projectService = projectService;
    }

    /**
     * run
     *
     * Metoda wywołująca się podczas tworzenia się Bean'a.
     * Dzięki czemu zapewnia nam przykładowe dane w bazie na czas używania aplikacji.
     * Tworzy przykładowego użytkownika i kilka obiektów Project.
     * @see pl.gatetologic.backend.springbootbackend.domain
     *
     * @param args Argumenty programu
     * @throws Exception {@link CommandLineRunner}
     */
    @Override
    public void run(String... args) throws Exception {

//        User user = User.builder().login(LOGIN).password(PASSWORD)
//                .email(EMAIL).projects(List.of()).build();
//
//        userService.save(user);
//

        Project project1 = Project.builder().id(ID).name(NAME).description(SOME_DESCRIPTION).representation(REPRESENTATION).build();
        Project project2 = Project.builder().id(ID2).name(NAME + "2").description(SOME_DESCRIPTION + "2").representation(REPRESENTATION).build();

        ProjectDto projectDto = ProjectDto.builder().id(ID).name(NAME).description(SOME_DESCRIPTION).representation(REPRESENTATION).build();


        projectService.save(project1);
        projectService.save(project2);
//        projectService.saveDTOToUser(projectDto, LOGIN);
    }
}
