package pl.gatetologic.backend.springbootbackend.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.service.declaration.UserService;


/**
 * Serwis implementujący po interfejsie UserDetailsService.
 * Jego zadaniem jest ładowanie specyficznych dla użytkownika danych - login i hasło.
 *
 *
 * @see org.springframework.security.core.userdetails.UserDetailsService
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {
    private UserService userService;

    @Autowired
    public JwtUserDetailsService(UserService userService) { this.userService = userService;}

    /**
     * Metoda znajduje użytkownika po loginie.
     *
     * @param username Login użytkownika.
     * @return Użytkownik jako obiekt klasy @link org.springframework.security.core.userdetails.User
     * @throws UsernameNotFoundException
     *
     * @see org.springframework.security.core.userdetails.User
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDto userDto = userService.findByLogin(username);
        if (userDto == null) {
            throw new UsernameNotFoundException("Użytkownik o podanym loginie nie istnieje: " + username);
        }
        return new org.springframework.security.core.userdetails.User(userDto.getLogin(), userDto.getPassword(),
                new ArrayList<>());
    }
}
