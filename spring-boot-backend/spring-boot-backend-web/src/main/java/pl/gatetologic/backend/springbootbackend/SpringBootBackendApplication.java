package pl.gatetologic.backend.springbootbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories
@SpringBootApplication
public class SpringBootBackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootBackendApplication.class, args);
    }
}
