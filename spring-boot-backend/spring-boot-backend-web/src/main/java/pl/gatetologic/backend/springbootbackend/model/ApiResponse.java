package pl.gatetologic.backend.springbootbackend.model;

import lombok.Getter;

/**
 * Klasa reprezentująca wysyłaną odpowiedź.
 * Zawiera ona pola informujące o sukcesie oraz wiadomości.
 * Implementuje interfejst Response.
 * @see pl.gatetologic.backend.springbootbackend.model.Response
 */
@Getter
public class ApiResponse implements Response{
    private Boolean success;
    private String message;

    /**
     * Konstrkutor z dwoma parametrami.
     * @param success Sukces
     * @param message Wiadomośc
     */
    public ApiResponse(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }
}
