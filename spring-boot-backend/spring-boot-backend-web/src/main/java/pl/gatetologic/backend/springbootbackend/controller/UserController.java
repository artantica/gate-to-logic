package pl.gatetologic.backend.springbootbackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import pl.gatetologic.backend.springbootbackend.ReqestModel.LoginRequest;
import pl.gatetologic.backend.springbootbackend.ReqestModel.RegisterRequest;
import pl.gatetologic.backend.springbootbackend.config.JwtTokenUtil;
import pl.gatetologic.backend.springbootbackend.domain.User;
import pl.gatetologic.backend.springbootbackend.domain.VerificationToken;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.email.MailJetEmail;
import pl.gatetologic.backend.springbootbackend.model.ApiResponse;
import pl.gatetologic.backend.springbootbackend.model.JwtResponse;
import pl.gatetologic.backend.springbootbackend.model.Response;
import pl.gatetologic.backend.springbootbackend.service.JwtUserDetailsService;
import pl.gatetologic.backend.springbootbackend.service.declaration.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Klasa odpowiadająca za obsługę zapytań dotyczących logowania i rejestracji użytkownika.
 * Adnotacja @RestController oznacza kontroler, tj. klasa, która będzie obsługiwała zapytania
 * wysyłane poprzez przeglądarkę od użytkowników.
 * Adnotacja @CrossOrigin() pozwala na wykonanie żądania HTTP na zastrzeżonych zasobach z serwera o innej domenie
 * niż domena serwera, który udostępnia te zasoby, dzięku czemu obsłużone zostanie żądanie z naszego frontendu w Angularze.
 *
 * @see org.springframework.web.bind.annotation.RestController
 * @see org.springframework.web.bind.annotation.CrossOrigin
 */
@RestController
@CrossOrigin()
public class UserController {

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String EMAIL_TOKEN = "emailToken";

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final PasswordEncoder passwordEncoder;
    private final JwtUserDetailsService userDetailsService;
    private final MailJetEmail mailJetEmail;


    @Autowired
    public UserController(UserService userService,
                          JwtUserDetailsService userDetailsService,
                          PasswordEncoder passwordEncoder,
                          JwtTokenUtil jwtTokenUtil,
                          AuthenticationManager authenticationManager,
                          MailJetEmail mailJetEmail) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenUtil = jwtTokenUtil;
        this.authenticationManager = authenticationManager;
        this.mailJetEmail = mailJetEmail;
    }

    /**
     * Metodą obsługująca zapytanie POST /register i tworząca nowego uzytkownika.
     *
     * @param request dane do rejestracji
     * @return
     */
    @PostMapping("/register")
    public ResponseEntity<ApiResponse> register(@RequestBody RegisterRequest request,
                                                HttpServletRequest requests
    ){
        if(userService.existsByLogin(request.getLogin())) {
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Nazwa użytkownika jest już zajęta!"));
        }
        if(userService.existsByEmail(request.getEmail())) {
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Adres email jest już wykorzystany!"));
        }


        UserDto userDto = UserDto.builder().login(request.getLogin())
                .password(passwordEncoder.encode(request.getPassword()))
                .emailVerifyToken(new VerificationToken((Instant.now())))
                .projects(List.of())
                .email(request.getEmail()).build();

        userService.saveDTO(userDto);
        sendEmailWithVerification(userDto, requests);


        return ResponseEntity.ok(new ApiResponse(true, "Rejestracja zakończyła się sukcesem"));
    }

    /**
     * Metodą obsługująca zapytanie POST /login.
     * Sprawdza ona dane użtkownika w bazie i po poprawnej autoryzacji wysyła token JWT.
     * @param request Dane do logowania
     * @return ApiResponse Odpowiedź, która zawiera wiadomość i token JWT.
     * @throws Exception
     */
    @PostMapping("/login")
    public ResponseEntity<Response> login(@RequestBody LoginRequest request) throws Exception{
        authenticate(request.getLogin(), request.getPassword());
        if (!userService.isUserVerified(userService.findByLogin(request.getLogin()))) {
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Logowanie nie powiodło się. Uzytkownik nie zweryfikował maila"));
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getLogin());
        String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(true, "Logowanie zakończyło się sukcesem", token));
    }

    /**
     * Metodą obsługująca zapytanie GET /all-users.
     * Zwraca ona liste wszystkich użytkowników.
     * @return
     */
    @GetMapping("/all-users")
    public Collection<UserDto> getAllUsers() { return this.userService.getAllUsers(); }

    @PostMapping("/resend-mail-verification")
    public ResponseEntity<ApiResponse> resendMailVerification(@RequestParam(EMAIL) final String email, HttpServletRequest request) {
        if(!userService.existsByEmail(email)) {
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Adres email jest niepoprawny!"));
        }
        sendEmailWithVerification(userService.findUserByEmail(email), request);
        return ResponseEntity.ok(new ApiResponse(true, "Na mail: " + email + " został wysłany ponownie link aktywacyjny!"));
    }

    @PostMapping("/verify-mail")
    public ResponseEntity<Response> verifyMailByEmailVerificationToken(@RequestBody final String token) {
        UserDto userDto = userService.findUserByEmailVerifyToken(token);
        if (userDto == null || userService.existsByEmailVerifyToken(token)) {
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Link aktywacyjny jest niepoprawny lub wygasł!"));
        }
        userDto = userService.setUserVerified(userDto);
        userService.setUserEmailVerifyToken(userDto, VerificationToken.builder().build());

        return ResponseEntity.ok(new ApiResponse(true, "Gartulacje " + userDto.getLogin() + "! \nMail " + userDto.getEmail() + " został potwierdzony! Możesz się zalogować"));
    }

    /**
     * Metoda służąca do weryfikacji loginu i hasła.
     * @param username Login użytkownika.
     * @param password Hasło użytkownika.
     * @throws Exception
     */
    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    private void sendEmailWithVerification(UserDto userDto, HttpServletRequest request) {
        System.out.println(request.getServerPort());

        String appUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()+ "/verify-mail?emailToken=" + userDto.getEmailVerifyToken().getToken();
        mailJetEmail.sendActivationEmail(userDto.getEmail(), userDto.getLogin(), appUrl);
    }
}
