package pl.gatetologic.backend.springbootbackend.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.gatetologic.backend.springbootbackend.exception.ObjectNotFoundException;

/**
 * Wyjatek wyrzucany przez klasę serwisu przy nieznalezieniu obiektu o podanym id w bazie.
 * Adnotacja @ControllerAdvice @ControllerAdvice sprawia, że metody klasy są dzielone pomiędzy wszystkimi kontrolerami w aplikacji.
 * Adnoatacja @ExceptionHandler wskazuje, jaki wyjątek ma obsługiwać dana metoda.
 *
 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
 * @see org.springframework.web.bind.annotation.ControllerAdvice
 * @see org.springframework.web.bind.annotation.ExceptionHandler
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    /**
     * Handler dla wszystkich kontrolerów.
     * @param e Błąd.
     * @return
     */
    @ExceptionHandler({ObjectNotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(Exception e){
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }
}
