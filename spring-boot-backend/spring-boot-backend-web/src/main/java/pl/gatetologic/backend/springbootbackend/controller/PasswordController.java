package pl.gatetologic.backend.springbootbackend.controller;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.gatetologic.backend.springbootbackend.ReqestModel.ResetRequest;
import pl.gatetologic.backend.springbootbackend.domain.VerificationToken;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.email.MailJetEmail;
import pl.gatetologic.backend.springbootbackend.model.ApiResponse;
import pl.gatetologic.backend.springbootbackend.service.declaration.UserService;


/**
 * Klasa odpowiadająca za obsługę zapytań dotyczących zapomniania i resetwania hasła użytkownika.
 * Adnotacja @RestController oznacza kontroler, tj. klasa, która będzie obsługiwała zapytania
 * wysyłane poprzez przeglądarkę od użytkowników.
 * Adnotacja @CrossOrigin() pozwala na wykonanie żądania HTTP na zastrzeżonych zasobach z serwera o innej domenie
 * niż domena serwera, który udostępnia te zasoby, dzięku czemu obsłużone zostanie żądanie z naszego frontendu w Angularze.
 *
 * @see org.springframework.web.bind.annotation.RestController
 * @see org.springframework.web.bind.annotation.CrossOrigin
 */
@RestController
@CrossOrigin()
public class PasswordController {
    public static final String EMAIL = "email";
    private final UserService userService;
    private final MailJetEmail mailJetEmail;

    @Autowired
    public PasswordController(UserService userService, MailJetEmail mailJetEmail) {
        this.userService = userService;
        this.mailJetEmail = mailJetEmail;
    }

    /**
     * Metodą obsługująca zapytanie POST /forgot-password.
     * Jest odpowiedzialna za weryfikacje podanego maila i wysłanie wiadomości mailowej z linkiem z tokenem do przypomnienia hasła i przypisanie tego tokena do użytkowanika w bazie dancyh.
     * @param email Email użytkownika.
     * @param request
     * @return
     */
    @PostMapping("/forgot-password")
    public ResponseEntity<ApiResponse> forgotPassword(@RequestBody final String email, HttpServletRequest request) {
        // Lookup user in database by e-mail
        System.out.println(email);
        UserDto userDto = userService.findUserByEmail(email);

        // Update user with token and send it to database
        Instant now = Instant.now();
        Instant after= now.plus(Duration.ofDays(1));

        userService.setUserPasswordResetToken(userDto, VerificationToken.builder().expiryDate(Date.from(after)).token(UUID.randomUUID().toString()).build());

        String appUrl = request.getScheme() + "://" + request.getServerName()+ "/reset?token=" + userDto.getPasswordResetToken().getToken();
        mailJetEmail.sendResetPasswordLink(userDto.getEmail(), userDto.getLogin(), appUrl);

        return ResponseEntity.ok(new ApiResponse(true, "Mail z linkiem do resetowania hasła został wysłany do: " + userDto.getEmail()));

    }

    /**
     * Metodą obsługująca zapytanie GET /reset.
     * Sprawdza ona poprawnośc tokena.
     * @param token Token resetowania hasła
     * @return
     */
    @GetMapping("/reset")
    public ResponseEntity<ApiResponse> checkResetPasswordToken(@RequestBody String token) {
        UserDto userDto = userService.findUserByResetPasswordToken(token);
        if (userDto == null && userService.existsByPasswordResetToken(token)) { // Token found in DB
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Oops!  Wygląda na to, że link jest nieaktywny. :("));
        }

        return ResponseEntity.ok(new ApiResponse(true, "Poprawny token"));
    }

    /**
     * Metoda obsługująca zapytanie POST /reset.
     * Nadaje użytkownikowi o podanym tokenie nowe hasło
     * @param request Request data
     * @return ApisResponse zawierająca wiadomość.
     */
    @PostMapping("/reset")
    public ResponseEntity<ApiResponse> setNewPassword(@RequestBody ResetRequest request) {
        // Find the user associated with the reset token
        UserDto userDto = userService.findUserByResetPasswordToken(request.getToken());

        // This should always be non-null but we check just in case
        if (userDto != null || userService.existsByPasswordResetToken(request.getToken())) {
            String encoded = new BCryptPasswordEncoder().encode(request.getPassword());
            UserDto resetUser = userService.changeUserPassword(userDto, encoded);
            // Set the reset token to null so it cannot be used again
            userService.setUserPasswordResetToken(resetUser, VerificationToken.builder().build());
        } else {
            return ResponseEntity.badRequest().body(new ApiResponse(false, "Oops! Wygląda na to, że twój link jest niepoprawny lub stracił ważność."));
        }

        return ResponseEntity.ok(new ApiResponse(true, "Zmiana hasła przebiegła pomyślnie. Możesz sie teraz zalogować."));
    }
}
