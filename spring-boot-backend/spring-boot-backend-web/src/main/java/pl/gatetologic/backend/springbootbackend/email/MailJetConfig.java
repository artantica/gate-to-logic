package pl.gatetologic.backend.springbootbackend.email;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Klasa konfigurująca wysyłanie maili z przypomnieniem hasła.
 * Adnotacja @Value wczytuje konfiguracje do pola klasy.
 *
 * @see org.springframework.beans.factory.annotation.Value
 */
@Component
public class MailJetConfig {
    @Value("${mailJet.fromEmail}")
    private String mailJetFromEmail;

    @Value("${mailJet.fromName}")
    private String mailJetFromName;

    @Value("${mailJet.apiKey}")
    private String mailJetApiKey;

    @Value("${mailJet.apiSecret}")
    private String mailJetApiSecret;

    public MailJetConfig() {}

    public MailJetConfig(String mailJetFromEmail, String mailJetFromName, String mailJetApiKey, String mailJetApiSecret) {
        this.mailJetFromEmail = mailJetFromEmail;
        this.mailJetFromName = mailJetFromName;
        this.mailJetApiKey = mailJetApiKey;
        this.mailJetApiSecret = mailJetApiSecret;
    }

    public String getMailJetFromEmail() {
        return mailJetFromEmail;
    }

    public String getMailJetFromName() {
        return mailJetFromName;
    }

    public String getMailJetApiKey() {
        return mailJetApiKey;
    }

    public String getMailJetApiSecret() {
        return mailJetApiSecret;
    }

    @Override
    public String toString() {
        return "MailJetConfig{" +
                "mailJetFromEmail='" + mailJetFromEmail + '\'' +
                ", mailJetFromName='" + mailJetFromName + '\'' +
                ", mailJetApiKey='" + mailJetApiKey + '\'' +
                ", mailJetApiSecret='" + mailJetApiSecret + '\'' +
                '}';
    }
}
