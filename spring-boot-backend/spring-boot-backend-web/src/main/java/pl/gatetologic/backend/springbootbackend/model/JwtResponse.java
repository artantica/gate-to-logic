package pl.gatetologic.backend.springbootbackend.model;

import lombok.Getter;

/**
 * Klasa reprezentująca wysyłaną odpowiedź z tokenem JWT.
 * Zawiera ona pola informujące o sukcesie, wiadomości oraz tokenie JWT.
 * Implementuje interfejst Response.
 * @see pl.gatetologic.backend.springbootbackend.model.Response
 */
@Getter
public class JwtResponse implements Response{
    private Boolean success;
    private String message;
    private String jwtAccessToken;

    /**
     * Konstrkutor z trzema parametrami.
     * @param success Sukces
     * @param message Wiadomość
     * @param jwtAccessToken Token JWT
     */
    public JwtResponse(Boolean success, String message, String jwtAccessToken) {
        this.success = success;
        this.message = message;
        this.jwtAccessToken = jwtAccessToken;
    }
}
