package pl.gatetologic.backend.springbootbackend.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.service.declaration.UserService;

import javax.servlet.ServletContext;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
//@SpringApplicationConfiguration(classes = {MyApplication.class, WebSecurityConfig.class})
@WebAppConfiguration
//@TransactionConfiguration(defaultRollback = true)
//@Transactional(rollbackOn = Exception.class)
class UserControllerTest {
    public static final String ID = "1";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";

    @Mock
    UserService userService;

    @InjectMocks
    UserController userController;

    UserDto userDto;

    private MockMvc mvc;

//    @Before
//    public void setup() {
//        mvc = MockMvcBuilders
//                .standaloneSetup(userController)
//                .setControllerAdvice(new RestResponseEntityExceptionHandler())
//                .build();
//    }

//    @BeforeEach
//    void setUp() {
//        userDto = UserDto.builder()
//                .id(ID)
//                .login(LOGIN)
//                .password(PASSWORD)
//                .email(EMAIL)
//                .projects(List.of())
//                .build();
//    }


//    @Test
//    void register() throws Exception {
//        //nie działa, wzoruj się na testach z projectControler albo internet :)
//        String uri = "/register";
//        MvcResult mvcResult = this.mvc.perform(post(uri)
//                .param("login", LOGIN)
//                .param("email", EMAIL)
//                .param("password", PASSWORD))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(content().json("{'success': 'true', 'message':'Register sucesfully', " +
//                        "'jwtAuthenticationResponse': 'null'}"))
//                .andReturn();;
//    }
//    @Test
//    void login() {
//    }

//    @Test
//    void getAllUsers() {
//    }
}