package pl.gatetologic.backend.springbootbackend.service.implementation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.gatetologic.backend.springbootbackend.domain.User;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.exception.ObjectNotFoundException;
import pl.gatetologic.backend.springbootbackend.mapper.ProjectMapperImpl;
import pl.gatetologic.backend.springbootbackend.mapper.UserMapperImpl;
import pl.gatetologic.backend.springbootbackend.service.declaration.UserService;
import pl.gatetologic.backend.springbootbackend.testconfig.MongoTestConfiguration;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static pl.gatetologic.backend.springbootbackend.testconfig.MongoTestConfiguration.ID;
import static pl.gatetologic.backend.springbootbackend.testconfig.MongoTestConfiguration.NAME;


@DataMongoTest
@ContextConfiguration(classes = {MongoTestConfiguration.class,
        ProjectServiceImpl.class,
        ServiceProxy.class,
        UserServiceImpl.class,
        UserMapperImpl.class,
        ProjectMapperImpl.class,
})
@ExtendWith(SpringExtension.class)
public class UserServiceImplTest {

    @Autowired
    UserService userService;

    @Autowired
    MongoTemplate mongo;

    private User toSave;

    @BeforeEach
    void setUp() {
        toSave = User.builder().id(ID).login(NAME).projects(new ArrayList<>()).build();
        mongo.remove(new Query(), User.class);
    }

    @Test
    void findByIdFound() {
        mongo.save(toSave);

        UserDto returned = userService.findById(ID);

        assertNotNull(returned);
        assertEquals(ID, returned.getId());
    }

//    @Test
//    void findByIdThrowsObjectNotFoundException() {
//        assertThrows(ObjectNotFoundException.class, () -> userService.findById(ID));
//    }

    @Test
    void save() {
        UserDto returned = userService.save(toSave);

        assertThat(mongo.findAll(User.class))
                .extracting("id").containsOnly(ID);

        assertNotNull(returned);
        assertEquals(ID, returned.getId());
    }

    @Test
    void saveDTO(){
        UserDto dto = new UserDto();
        dto.setId(ID);
        dto.setProjects(new ArrayList<>());

        UserDto returned = userService.saveDTO(dto);

        assertThat(mongo.findAll(User.class))
                .extracting("id").containsOnly(ID);

        assertNotNull(returned);
        assertEquals(ID, returned.getId());
    }

    @Test
    void deleteByIdTheOnlyUser(){
        mongo.save(toSave);

        userService.deleteById(ID);

        assertThat(mongo.findAll(User.class)).isEmpty();
    }

    @Test
    void deleteByIdNotTheOnlyUser(){
        String checkedId = ID + "1";
        User toSave2 = User.builder().id(checkedId).login(NAME).build();

        mongo.save(toSave);
        mongo.save(toSave2);

        userService.deleteById(checkedId);
        assertThat(mongo.findAll(User.class))
                .hasSize(1)
                .doesNotContain(toSave2)
                .extracting(User::getId)
                .containsExactly(ID);
    }


    @Test
    void findByLoginFound() {
        mongo.save(toSave);

        UserDto returned = userService.findByLogin(NAME);

        assertNotNull(returned);
        assertEquals(NAME, returned.getLogin());
    }

//    @Test
//    void findByLoginThrowsObjectNotFoundException() {
//        assertThrows(ObjectNotFoundException.class, () -> userService.findByLogin(NAME));
//    }
}
