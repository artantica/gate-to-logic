package pl.gatetologic.backend.springbootbackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class ProjectControllerTest {
    public static final String ID = "1";
    public static final String NAME = "name";
    public static final String REPRESENTATION = "representation";
    public static final String DESCRIPTION = "description";

    @Mock
    ProjectService projectService;

    @InjectMocks
    ProjectController projectController;

    MockMvc mvc;
    ProjectDto projectDto;


    @BeforeEach
    void setUp() {
        projectDto = ProjectDto.builder()
                .id(ID)
                .name(NAME)
                .description(DESCRIPTION)
                .representation(REPRESENTATION)
                .build();

        mvc = MockMvcBuilders
                .standaloneSetup(projectController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

//    @Test
//    void findAllByUserLogin() throws Exception {
//        ProjectDto projectDto1 = ProjectDto.builder().id(ID + "1").build();
//        List<ProjectDto> returned = List.of(projectDto, projectDto1);
//
//        when(projectService.findAllByUserLogin(anyString())).thenReturn(returned);
//
//        String uri = "/project/all";
//        mvc.perform(get(uri)
////                .param()
//                .accept(MediaType.APPLICATION_JSON)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(2)));
//    }

    @Test
    void getProjectById() throws Exception {
        when(projectService.findById(anyString())).thenReturn(projectDto);

        String uri = "/project/get/project_id";
        mvc.perform(get(uri)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(NAME)));
    }

//    @Test
//    void addNewProject() throws Exception {
//        when(projectService.saveDTOToUser(any(ProjectDto.class), any(UserDto.class))).thenReturn(projectDto);
//        String uri = "/project/new";
//        mvc.perform(post(uri)
//                .accept(MediaType.APPLICATION_JSON)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(new ObjectMapper().writeValueAsString(projectDto))
//                .content(new ObjectMapper().writeValueAsString(UserDto.builder().build())))
//                .andExpect(status().isCreated())
//                .andExpect(jsonPath("$.name", equalTo(NAME)));
//    }

    @Test
    void patchSystemByProjectId() throws Exception {
        projectDto.setRepresentation(REPRESENTATION);
        when(projectService.patchRepresentationByProjectId(anyString(), anyString())).thenReturn(projectDto);

        String uri = "/project/save/project_id";
        mvc.perform(patch(uri)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(REPRESENTATION)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(ID)));
    }
}