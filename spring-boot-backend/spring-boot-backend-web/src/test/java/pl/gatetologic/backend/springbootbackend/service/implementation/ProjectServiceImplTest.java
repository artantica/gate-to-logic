package pl.gatetologic.backend.springbootbackend.service.implementation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.gatetologic.backend.springbootbackend.domain.Project;
import pl.gatetologic.backend.springbootbackend.domain.User;
import pl.gatetologic.backend.springbootbackend.dto.ProjectDto;
import pl.gatetologic.backend.springbootbackend.dto.UserDto;
import pl.gatetologic.backend.springbootbackend.exception.ObjectNotFoundException;
import pl.gatetologic.backend.springbootbackend.mapper.*;
import pl.gatetologic.backend.springbootbackend.service.declaration.ProjectService;
import pl.gatetologic.backend.springbootbackend.testconfig.MongoTestConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static pl.gatetologic.backend.springbootbackend.testconfig.MongoTestConfiguration.*;

@DataMongoTest
@ContextConfiguration(classes = {MongoTestConfiguration.class,
        ProjectServiceImpl.class,
        ServiceProxy.class,
        UserServiceImpl.class,
        UserMapperImpl.class,
        ProjectMapperImpl.class,
})
@ExtendWith(SpringExtension.class)
//@AutoConfigureRestDocs(outputDir = "target/snippets")
public class ProjectServiceImplTest {

    @Autowired
    ProjectService projectService;

    @Autowired
    MongoTemplate mongo;

    private Project toSave;

    @BeforeEach
    void setUp() {
        toSave = Project.builder().id(ID).name(NAME).build();

        mongo.remove(new Query(), Project.class);
        mongo.remove(new Query(), User.class);
    }

    @Test
    void findAllFound() {
        Project toSave2 = Project.builder().id(ID + "1").name(NAME).build();
        mongo.save(toSave);
        mongo.save(toSave2);

        List<ProjectDto> returned = projectService.findAll();
        assertNotNull(returned);
        assertEquals(2, returned.size());
        assertNotEquals(returned.get(0), returned.get(1));
    }

    @Test
    void findAllNotFound() {
        List<ProjectDto> returned = projectService.findAll();

        assertNotNull(returned);
        assertEquals(0, returned.size());
    }

    @Test
    void findAllByUserLoginFound() {
        String checkedId = ID + "2";
        Project toSave2 = Project.builder().id(ID + "1").name(NAME).build();
        Project toSave3 = Project.builder().id(checkedId).name(NAME).build();
        mongo.save(toSave);
        mongo.save(toSave2);
        mongo.save(toSave3);
        User user = User.builder().id(ID).login(NAME).projects(List.of(toSave, toSave2)).build();
        mongo.save(user);

        List<ProjectDto> returned = projectService.findAllByUserLogin(NAME);

        assertNotNull(returned);
        assertEquals(2, returned.size());
        assertNotEquals(checkedId, returned.get(0).getId());
        assertNotEquals(checkedId, returned.get(1).getId());
    }

    @Test
    void findAllByUserLoginNotFoundProjects() {
        User user = User.builder().id(ID).login(NAME).build();
        mongo.save(user);

        List<ProjectDto> returned = projectService.findAllByUserLogin(NAME);

        assertNotNull(returned);
        assertEquals(0, returned.size());
    }

//    @Test
//    void findAllByUserLoginThrowsObjectNotFoundException() {
//        assertThrows(ObjectNotFoundException.class, () -> projectService.findAllByUserLogin(NAME));
//    }

    @Test
    void findByIdFound() {
        mongo.save(toSave);

        ProjectDto returned = projectService.findById(ID);

        assertNotNull(returned);
        assertEquals(ID, returned.getId());
    }

    @Test
    void findByIdThrowsObjectNotFoundException() {
        assertThrows(ObjectNotFoundException.class, () -> projectService.findById(ID));
    }

    @Test
    void save() {
        ProjectDto returned = projectService.save(toSave);

        assertThat(mongo.findAll(Project.class))
                .extracting("id").containsOnly(ID);

        assertNotNull(returned);
        assertEquals(ID, returned.getId());
    }

    @Test
    void saveDTO() {
        ProjectDto dto = new ProjectDto();
        dto.setId(ID);

        ProjectDto returned = projectService.saveDTO(dto);

        assertThat(mongo.findAll(Project.class))
                .extracting("id").containsOnly(ID);

        assertNotNull(returned);
        assertEquals(ID, returned.getId());
    }

    @Test
    void saveDtoToUserFoundUser() {
        UserDto user = UserDto.builder()
                .id(ID)
                .login(NAME)
                .projects(new ArrayList<>())
                .build();
        mongo.save(user);

        ProjectDto toSaveDto = ProjectDto.builder()
                .id(ID)
                .name(NAME)
                .build();
        ProjectDto returned = projectService.saveDTOToUser(toSaveDto, user);

        assertThat(mongo.findAll(Project.class))
                .extracting("id")
                .containsExactly(ID);
        assertThat(mongo.findAll(User.class))
                .hasSize(1);
        assertThat(mongo.findAll(User.class).get(0).getProjects())
                .hasSize(1)
                .extracting(Project::getId)
                .containsExactly(ID);
        assertNotNull(returned);
        assertEquals(ID, returned.getId());
    }

    @Test
    void deleteByIdNotTheOnlyProject() {
        String checkedId = ID + "1";
        Project toSave2 = Project.builder().id(checkedId).name(NAME).build();

        mongo.save(toSave);
        mongo.save(toSave2);

        projectService.deleteById(checkedId);
        assertThat(mongo.findAll(Project.class))
                .hasSize(1)
                .containsExactly(toSave)
                .doesNotContain(toSave2);
    }

    @Test
    void deleteByIdTheOnlyProject() {
        mongo.save(toSave);

        projectService.deleteById(ID);

        assertThat(mongo.findAll(Project.class)).isEmpty();
    }

//    @Test
//    void patchSystemByProjectId() {
//        mongo.save(toSave);
//
//        ProjectDto returned = projectService.patchRepresentationByProjectId(REPRESENTATION, ID);
//
//        assertNotNull(returned);
//        assertEquals(STRING, returned.getRepresentation());
//    }


}