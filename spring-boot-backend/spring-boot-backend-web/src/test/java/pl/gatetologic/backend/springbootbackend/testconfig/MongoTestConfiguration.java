package pl.gatetologic.backend.springbootbackend.testconfig;

import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import pl.gatetologic.backend.springbootbackend.mapper.ProjectMapper;
import pl.gatetologic.backend.springbootbackend.mapper.UserMapper;
import pl.gatetologic.backend.springbootbackend.repository.ProjectRepository;
import pl.gatetologic.backend.springbootbackend.repository.UserRepository;
import pl.gatetologic.backend.springbootbackend.service.implementation.ProjectServiceImpl;
import pl.gatetologic.backend.springbootbackend.service.implementation.ServiceProxy;
import pl.gatetologic.backend.springbootbackend.service.implementation.UserServiceImpl;

import java.io.IOException;

@TestConfiguration
@EnableMongoRepositories(basePackages = "pl.gatetologic.backend.springbootbackend.repository")
public class MongoTestConfiguration {
    private static final String IP = "localhost";
    private static final int PORT = 28017;

    public static final String ID = "1";
    public static final String NAME = "name";
    public static final String REPRESENTATION = "representation";
    public static final String PASSWORD = "pwd";
    public static final String STRING = "string";
    public static final String EMAIL = "email@email.com";

    @Bean
    public IMongodConfig embeddedMongoConfiguration() throws IOException {
        return new MongodConfigBuilder()
                .version(Version.V4_0_2) // <- set MongoDB version
                .net(new Net(IP, PORT, Network.localhostIsIPv6()))
                .build();
    }
}
